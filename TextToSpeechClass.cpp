/*----- PROTECTED REGION ID(TextToSpeechClass.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        TextToSpeechClass.cpp
//
// description : C++ source for the TextToSpeechClass.
//               A singleton class derived from DeviceClass.
//               It implements the command and attribute list
//               and all properties and methods required
//               by the TextToSpeech once per process.
//
// project :     TextToSpeech
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <TextToSpeechClass.h>

/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass.cpp

//-------------------------------------------------------------------
/**
 *	Create TextToSpeechClass singleton and
 *	return it in a C function for Python usage
 */
//-------------------------------------------------------------------
extern "C" {
#ifdef _TG_WINDOWS_

__declspec(dllexport)

#endif

	Tango::DeviceClass *_create_TextToSpeech_class(const char *name) {
		return TextToSpeech_ns::TextToSpeechClass::init(name);
	}
}

namespace TextToSpeech_ns
{
//===================================================================
//	Initialize pointer for singleton pattern
//===================================================================
TextToSpeechClass *TextToSpeechClass::_instance = NULL;

//--------------------------------------------------------
/**
 * method : 		TextToSpeechClass::TextToSpeechClass(string &s)
 * description : 	constructor for the TextToSpeechClass
 *
 * @param s	The class name
 */
//--------------------------------------------------------
TextToSpeechClass::TextToSpeechClass(string &s):Tango::DeviceClass(s)
{
	cout2 << "Entering TextToSpeechClass constructor" << endl;
	set_default_property();
	write_class_property();

	/*----- PROTECTED REGION ID(TextToSpeechClass::constructor) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass::constructor

	cout2 << "Leaving TextToSpeechClass constructor" << endl;
}

//--------------------------------------------------------
/**
 * method : 		TextToSpeechClass::~TextToSpeechClass()
 * description : 	destructor for the TextToSpeechClass
 */
//--------------------------------------------------------
TextToSpeechClass::~TextToSpeechClass()
{
	/*----- PROTECTED REGION ID(TextToSpeechClass::destructor) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass::destructor

	_instance = NULL;
}


//--------------------------------------------------------
/**
 * method : 		TextToSpeechClass::init
 * description : 	Create the object if not already done.
 *                  Otherwise, just return a pointer to the object
 *
 * @param	name	The class name
 */
//--------------------------------------------------------
TextToSpeechClass *TextToSpeechClass::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			string s(name);
			_instance = new TextToSpeechClass(s);
		}
		catch (bad_alloc &)
		{
			throw;
		}
	}
	return _instance;
}

//--------------------------------------------------------
/**
 * method : 		TextToSpeechClass::instance
 * description : 	Check if object already created,
 *                  and return a pointer to the object
 */
//--------------------------------------------------------
TextToSpeechClass *TextToSpeechClass::instance()
{
	if (_instance == NULL)
	{
		cerr << "Class is not initialised !!" << endl;
		exit(-1);
	}
	return _instance;
}



//===================================================================
//	Command execution method calls
//===================================================================
//--------------------------------------------------------
/**
 * method : 		OffClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *OffClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "OffClass::execute(): arrived" << endl;
	((static_cast<TextToSpeech *>(device))->off());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		OnClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *OnClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "OnClass::execute(): arrived" << endl;
	((static_cast<TextToSpeech *>(device))->on());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		ResetClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *ResetClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "ResetClass::execute(): arrived" << endl;
	((static_cast<TextToSpeech *>(device))->reset());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		DevClearClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *DevClearClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "DevClearClass::execute(): arrived" << endl;
	((static_cast<TextToSpeech *>(device))->dev_clear());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		DevStopClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *DevStopClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "DevStopClass::execute(): arrived" << endl;
	((static_cast<TextToSpeech *>(device))->dev_stop());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		DevTalkClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *DevTalkClass::execute(Tango::DeviceImpl *device, const CORBA::Any &in_any)
{
	cout2 << "DevTalkClass::execute(): arrived" << endl;
	Tango::DevString argin;
	extract(in_any, argin);
	((static_cast<TextToSpeech *>(device))->dev_talk(argin));
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		ClearStatsClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *ClearStatsClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "ClearStatsClass::execute(): arrived" << endl;
	((static_cast<TextToSpeech *>(device))->clear_stats());
	return new CORBA::Any();
}


//===================================================================
//	Properties management
//===================================================================
//--------------------------------------------------------
/**
 *	Method      : TextToSpeechClass::get_class_property()
 *	Description : Get the class property for specified name.
 */
//--------------------------------------------------------
Tango::DbDatum TextToSpeechClass::get_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, returns  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//--------------------------------------------------------
/**
 *	Method      : TextToSpeechClass::get_default_device_property()
 *	Description : Return the default value for device property.
 */
//--------------------------------------------------------
Tango::DbDatum TextToSpeechClass::get_default_device_property(string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//--------------------------------------------------------
/**
 *	Method      : TextToSpeechClass::get_default_class_property()
 *	Description : Return the default value for class property.
 */
//--------------------------------------------------------
Tango::DbDatum TextToSpeechClass::get_default_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}


//--------------------------------------------------------
/**
 *	Method      : TextToSpeechClass::set_default_property()
 *	Description : Set default property (class and device) for wizard.
 *                For each property, add to wizard property name and description.
 *                If default value has been set, add it to wizard property and
 *                store it in a DbDatum.
 */
//--------------------------------------------------------
void TextToSpeechClass::set_default_property()
{
	string	prop_name;
	string	prop_desc;
	string	prop_def;
	vector<string>	vect_data;

	//	Set Default Class Properties

	//	Set Default device Properties
	prop_name = "StartText";
	prop_desc = "This text will be spoken at the initialization of the server.";
	prop_def  = "Tango text to speech ready.";
	vect_data.clear();
	vect_data.push_back("Tango text to speech ready.");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "FileCacheLocation";
	prop_desc = "Optional parameter to be used when the CacheType is set to File. This specifies the path to use as a cache location. Default is the device server directory";
	prop_def  = "cache";
	vect_data.clear();
	vect_data.push_back("cache");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "CacheType";
	prop_desc = "Type of cache to use when caching speech. Supported parameters are ``None`` and ``File``";
	prop_def  = "File";
	vect_data.clear();
	vect_data.push_back("File");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "JingleDirectory";
	prop_desc = "Location of files to be used as jingles";
	prop_def  = "jingles";
	vect_data.clear();
	vect_data.push_back("jingles");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "FallbackText";
	prop_desc = "Text used to inform the operator there is a connectivity issue when trying to reach Amazon Polly Services.";
	prop_def  = "Unable to connect to the Amazon Polly Service. Please check connectivity.";
	vect_data.clear();
	vect_data.push_back("Unable to connect to the Amazon Polly Service. Please check connectivity.");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
}

//--------------------------------------------------------
/**
 *	Method      : TextToSpeechClass::write_class_property()
 *	Description : Set class description fields as property in database
 */
//--------------------------------------------------------
void TextToSpeechClass::write_class_property()
{
	//	First time, check if database used
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	string	classname = get_name();
	string	header;
	string::size_type	start, end;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	string	str_title("Text To Speech Server");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	vector<string>	str_desc;
	str_desc.push_back("");
	description << str_desc;
	data.push_back(description);

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	vector<string> inheritance;
	inheritance.push_back("TANGO_BASE_CLASS");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	get_db_class()->put_property(data);
}

//===================================================================
//	Factory methods
//===================================================================

//--------------------------------------------------------
/**
 *	Method      : TextToSpeechClass::device_factory()
 *	Description : Create the device object(s)
 *                and store them in the device list
 */
//--------------------------------------------------------
void TextToSpeechClass::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{
	/*----- PROTECTED REGION ID(TextToSpeechClass::device_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass::device_factory_before

	//	Create devices and add it into the device list
	for (unsigned long i=0 ; i<devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
		device_list.push_back(new TextToSpeech(this, (*devlist_ptr)[i]));
	}

	//	Manage dynamic attributes if any
	erase_dynamic_attributes(devlist_ptr, get_class_attr()->get_attr_list());

	//	Export devices to the outside world
	for (unsigned long i=1 ; i<=devlist_ptr->length() ; i++)
	{
		//	Add dynamic attributes if any
		TextToSpeech *dev = static_cast<TextToSpeech *>(device_list[device_list.size()-i]);
		dev->add_dynamic_attributes();

		//	Check before if database used.
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(dev);
		else
			export_device(dev, dev->get_name().c_str());
	}

	/*----- PROTECTED REGION ID(TextToSpeechClass::device_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass::device_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : TextToSpeechClass::attribute_factory()
 *	Description : Create the attribute object(s)
 *                and store them in the attribute list
 */
//--------------------------------------------------------
void TextToSpeechClass::attribute_factory(vector<Tango::Attr *> &att_list)
{
	/*----- PROTECTED REGION ID(TextToSpeechClass::attribute_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass::attribute_factory_before
	//	Attribute : text_to_talk
	text_to_talkAttrib	*text_to_talk = new text_to_talkAttrib();
	Tango::UserDefaultAttrProp	text_to_talk_prop;
	text_to_talk_prop.set_description("The text to speak.");
	text_to_talk_prop.set_label("Text to talk");
	//	unit	not set for text_to_talk
	//	standard_unit	not set for text_to_talk
	//	display_unit	not set for text_to_talk
	//	format	not set for text_to_talk
	//	max_value	not set for text_to_talk
	//	min_value	not set for text_to_talk
	//	max_alarm	not set for text_to_talk
	//	min_alarm	not set for text_to_talk
	//	max_warning	not set for text_to_talk
	//	min_warning	not set for text_to_talk
	//	delta_t	not set for text_to_talk
	//	delta_val	not set for text_to_talk
	
	text_to_talk->set_default_properties(text_to_talk_prop);
	//	Not Polled
	text_to_talk->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	text_to_talk->set_change_event(true, false);
	text_to_talk->set_archive_event(true, false);
	att_list.push_back(text_to_talk);

	//	Attribute : voice
	voiceAttrib	*voice = new voiceAttrib();
	Tango::UserDefaultAttrProp	voice_prop;
	voice_prop.set_description("Available voices returned from the text to speech library.");
	voice_prop.set_label("Select Voice");
	//	unit	not set for voice
	//	standard_unit	not set for voice
	//	display_unit	not set for voice
	//	format	not set for voice
	//	max_value	not set for voice
	//	min_value	not set for voice
	//	max_alarm	not set for voice
	//	min_alarm	not set for voice
	//	max_warning	not set for voice
	//	min_warning	not set for voice
	//	delta_t	not set for voice
	//	delta_val	not set for voice
	
	voice->set_default_properties(voice_prop);
	//	Not Polled
	voice->set_disp_level(Tango::OPERATOR);
	voice->set_memorized();
	voice->set_memorized_init(true);
	att_list.push_back(voice);

	//	Attribute : jingle
	jingleAttrib	*jingle = new jingleAttrib();
	Tango::UserDefaultAttrProp	jingle_prop;
	jingle_prop.set_description("Currently available jingles in the jingle directory (raw pcm files only).");
	jingle_prop.set_label("Select Jingle");
	//	unit	not set for jingle
	//	standard_unit	not set for jingle
	//	display_unit	not set for jingle
	//	format	not set for jingle
	//	max_value	not set for jingle
	//	min_value	not set for jingle
	//	max_alarm	not set for jingle
	//	min_alarm	not set for jingle
	//	max_warning	not set for jingle
	//	min_warning	not set for jingle
	//	delta_t	not set for jingle
	//	delta_val	not set for jingle
	
	jingle->set_default_properties(jingle_prop);
	//	Not Polled
	jingle->set_disp_level(Tango::OPERATOR);
	jingle->set_memorized();
	jingle->set_memorized_init(true);
	att_list.push_back(jingle);

	//	Attribute : speech_delay
	speech_delayAttrib	*speech_delay = new speech_delayAttrib();
	Tango::UserDefaultAttrProp	speech_delay_prop;
	speech_delay_prop.set_description("Time in milliseconds between a jingle finishing playback and speech starting.");
	speech_delay_prop.set_label("Speech Delay");
	speech_delay_prop.set_unit("ms");
	speech_delay_prop.set_standard_unit("ms");
	speech_delay_prop.set_display_unit("ms");
	//	format	not set for speech_delay
	speech_delay_prop.set_max_value("1000");
	speech_delay_prop.set_min_value("0");
	//	max_alarm	not set for speech_delay
	//	min_alarm	not set for speech_delay
	//	max_warning	not set for speech_delay
	//	min_warning	not set for speech_delay
	//	delta_t	not set for speech_delay
	//	delta_val	not set for speech_delay
	
	speech_delay->set_default_properties(speech_delay_prop);
	//	Not Polled
	speech_delay->set_disp_level(Tango::OPERATOR);
	speech_delay->set_memorized();
	speech_delay->set_memorized_init(true);
	att_list.push_back(speech_delay);

	//	Attribute : cache_hits
	cache_hitsAttrib	*cache_hits = new cache_hitsAttrib();
	Tango::UserDefaultAttrProp	cache_hits_prop;
	cache_hits_prop.set_description("Total number of cache hits");
	cache_hits_prop.set_label("Cache Hits");
	cache_hits_prop.set_unit("hits");
	cache_hits_prop.set_standard_unit("hits");
	cache_hits_prop.set_display_unit("hits");
	//	format	not set for cache_hits
	//	max_value	not set for cache_hits
	cache_hits_prop.set_min_value("0");
	//	max_alarm	not set for cache_hits
	//	min_alarm	not set for cache_hits
	//	max_warning	not set for cache_hits
	//	min_warning	not set for cache_hits
	//	delta_t	not set for cache_hits
	//	delta_val	not set for cache_hits
	
	cache_hits->set_default_properties(cache_hits_prop);
	//	Not Polled
	cache_hits->set_disp_level(Tango::EXPERT);
	//	Not Memorized
	cache_hits->set_archive_event(true, false);
	att_list.push_back(cache_hits);

	//	Attribute : polly_requests
	polly_requestsAttrib	*polly_requests = new polly_requestsAttrib();
	Tango::UserDefaultAttrProp	polly_requests_prop;
	polly_requests_prop.set_description("Total number of polly speech requests");
	polly_requests_prop.set_label("Polly Requests");
	polly_requests_prop.set_unit("requests");
	polly_requests_prop.set_standard_unit("requests");
	polly_requests_prop.set_display_unit("requests");
	//	format	not set for polly_requests
	//	max_value	not set for polly_requests
	polly_requests_prop.set_min_value("0");
	//	max_alarm	not set for polly_requests
	//	min_alarm	not set for polly_requests
	//	max_warning	not set for polly_requests
	//	min_warning	not set for polly_requests
	//	delta_t	not set for polly_requests
	//	delta_val	not set for polly_requests
	
	polly_requests->set_default_properties(polly_requests_prop);
	//	Not Polled
	polly_requests->set_disp_level(Tango::EXPERT);
	//	Not Memorized
	polly_requests->set_archive_event(true, false);
	att_list.push_back(polly_requests);

	//	Attribute : total_requests
	total_requestsAttrib	*total_requests = new total_requestsAttrib();
	Tango::UserDefaultAttrProp	total_requests_prop;
	total_requests_prop.set_description("Total speech requests");
	total_requests_prop.set_label("Total Requests");
	total_requests_prop.set_unit("requests");
	total_requests_prop.set_standard_unit("requests");
	total_requests_prop.set_display_unit("requests");
	//	format	not set for total_requests
	//	max_value	not set for total_requests
	total_requests_prop.set_min_value("0");
	//	max_alarm	not set for total_requests
	//	min_alarm	not set for total_requests
	//	max_warning	not set for total_requests
	//	min_warning	not set for total_requests
	//	delta_t	not set for total_requests
	//	delta_val	not set for total_requests
	
	total_requests->set_default_properties(total_requests_prop);
	//	Not Polled
	total_requests->set_disp_level(Tango::EXPERT);
	//	Not Memorized
	total_requests->set_archive_event(true, false);
	att_list.push_back(total_requests);

	//	Attribute : failed_requests
	failed_requestsAttrib	*failed_requests = new failed_requestsAttrib();
	Tango::UserDefaultAttrProp	failed_requests_prop;
	failed_requests_prop.set_description("Requests which have either failed or been terminated");
	failed_requests_prop.set_label("Failed Requests");
	failed_requests_prop.set_unit("requests");
	failed_requests_prop.set_standard_unit("requests");
	failed_requests_prop.set_display_unit("requests");
	//	format	not set for failed_requests
	//	max_value	not set for failed_requests
	failed_requests_prop.set_min_value("0");
	//	max_alarm	not set for failed_requests
	//	min_alarm	not set for failed_requests
	//	max_warning	not set for failed_requests
	//	min_warning	not set for failed_requests
	//	delta_t	not set for failed_requests
	//	delta_val	not set for failed_requests
	
	failed_requests->set_default_properties(failed_requests_prop);
	//	Not Polled
	failed_requests->set_disp_level(Tango::EXPERT);
	//	Not Memorized
	failed_requests->set_archive_event(true, false);
	att_list.push_back(failed_requests);

	//	Attribute : successful_requests
	successful_requestsAttrib	*successful_requests = new successful_requestsAttrib();
	Tango::UserDefaultAttrProp	successful_requests_prop;
	successful_requests_prop.set_description("Total requests that were successful.");
	successful_requests_prop.set_label("Successful Requests");
	successful_requests_prop.set_unit("requests");
	successful_requests_prop.set_standard_unit("requests");
	successful_requests_prop.set_display_unit("requests");
	//	format	not set for successful_requests
	//	max_value	not set for successful_requests
	successful_requests_prop.set_min_value("0");
	//	max_alarm	not set for successful_requests
	//	min_alarm	not set for successful_requests
	//	max_warning	not set for successful_requests
	//	min_warning	not set for successful_requests
	//	delta_t	not set for successful_requests
	//	delta_val	not set for successful_requests
	
	successful_requests->set_default_properties(successful_requests_prop);
	//	Not Polled
	successful_requests->set_disp_level(Tango::EXPERT);
	//	Not Memorized
	successful_requests->set_archive_event(true, false);
	att_list.push_back(successful_requests);


	//	Create a list of static attributes
	create_static_attribute_list(get_class_attr()->get_attr_list());
	/*----- PROTECTED REGION ID(TextToSpeechClass::attribute_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass::attribute_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : TextToSpeechClass::pipe_factory()
 *	Description : Create the pipe object(s)
 *                and store them in the pipe list
 */
//--------------------------------------------------------
void TextToSpeechClass::pipe_factory()
{
	/*----- PROTECTED REGION ID(TextToSpeechClass::pipe_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass::pipe_factory_before
	/*----- PROTECTED REGION ID(TextToSpeechClass::pipe_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass::pipe_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : TextToSpeechClass::command_factory()
 *	Description : Create the command object(s)
 *                and store them in the command list
 */
//--------------------------------------------------------
void TextToSpeechClass::command_factory()
{
	/*----- PROTECTED REGION ID(TextToSpeechClass::command_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass::command_factory_before


	//	Command Off
	OffClass	*pOffCmd =
		new OffClass("Off",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"None",
			"None",
			Tango::OPERATOR);
	command_list.push_back(pOffCmd);

	//	Command On
	OnClass	*pOnCmd =
		new OnClass("On",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"None",
			"None",
			Tango::OPERATOR);
	command_list.push_back(pOnCmd);

	//	Command Reset
	ResetClass	*pResetCmd =
		new ResetClass("Reset",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"None",
			"None",
			Tango::OPERATOR);
	command_list.push_back(pResetCmd);

	//	Command DevClear
	DevClearClass	*pDevClearCmd =
		new DevClearClass("DevClear",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"None",
			"None",
			Tango::OPERATOR);
	command_list.push_back(pDevClearCmd);

	//	Command DevStop
	DevStopClass	*pDevStopCmd =
		new DevStopClass("DevStop",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"None",
			"None",
			Tango::OPERATOR);
	command_list.push_back(pDevStopCmd);

	//	Command DevTalk
	DevTalkClass	*pDevTalkCmd =
		new DevTalkClass("DevTalk",
			Tango::DEV_STRING, Tango::DEV_VOID,
			"Text to be spoken",
			"None",
			Tango::OPERATOR);
	command_list.push_back(pDevTalkCmd);

	//	Command ClearStats
	ClearStatsClass	*pClearStatsCmd =
		new ClearStatsClass("ClearStats",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::EXPERT);
	command_list.push_back(pClearStatsCmd);

	/*----- PROTECTED REGION ID(TextToSpeechClass::command_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass::command_factory_after
}

//===================================================================
//	Dynamic attributes related methods
//===================================================================

//--------------------------------------------------------
/**
 * method : 		TextToSpeechClass::create_static_attribute_list
 * description : 	Create the a list of static attributes
 *
 * @param	att_list	the ceated attribute list
 */
//--------------------------------------------------------
void TextToSpeechClass::create_static_attribute_list(vector<Tango::Attr *> &att_list)
{
	for (unsigned long i=0 ; i<att_list.size() ; i++)
	{
		string att_name(att_list[i]->get_name());
		transform(att_name.begin(), att_name.end(), att_name.begin(), ::tolower);
		defaultAttList.push_back(att_name);
	}

	cout2 << defaultAttList.size() << " attributes in default list" << endl;

	/*----- PROTECTED REGION ID(TextToSpeechClass::create_static_att_list) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass::create_static_att_list
}


//--------------------------------------------------------
/**
 * method : 		TextToSpeechClass::erase_dynamic_attributes
 * description : 	delete the dynamic attributes if any.
 *
 * @param	devlist_ptr	the device list pointer
 * @param	list of all attributes
 */
//--------------------------------------------------------
void TextToSpeechClass::erase_dynamic_attributes(const Tango::DevVarStringArray *devlist_ptr, vector<Tango::Attr *> &att_list)
{
	Tango::Util *tg = Tango::Util::instance();

	for (unsigned long i=0 ; i<devlist_ptr->length() ; i++)
	{
		Tango::DeviceImpl *dev_impl = tg->get_device_by_name(((string)(*devlist_ptr)[i]).c_str());
		TextToSpeech *dev = static_cast<TextToSpeech *> (dev_impl);

		vector<Tango::Attribute *> &dev_att_list = dev->get_device_attr()->get_attribute_list();
		vector<Tango::Attribute *>::iterator ite_att;
		for (ite_att=dev_att_list.begin() ; ite_att != dev_att_list.end() ; ++ite_att)
		{
			string att_name((*ite_att)->get_name_lower());
			if ((att_name == "state") || (att_name == "status"))
				continue;
			vector<string>::iterator ite_str = find(defaultAttList.begin(), defaultAttList.end(), att_name);
			if (ite_str == defaultAttList.end())
			{
				cout2 << att_name << " is a UNWANTED dynamic attribute for device " << (*devlist_ptr)[i] << endl;
				Tango::Attribute &att = dev->get_device_attr()->get_attr_by_name(att_name.c_str());
				dev->remove_attribute(att_list[att.get_attr_idx()], true, false);
				--ite_att;
			}
		}
	}
	/*----- PROTECTED REGION ID(TextToSpeechClass::erase_dynamic_attributes) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass::erase_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Method      : TextToSpeechClass::get_attr_object_by_name()
 *	Description : returns Tango::Attr * object found by name
 */
//--------------------------------------------------------
Tango::Attr *TextToSpeechClass::get_attr_object_by_name(vector<Tango::Attr *> &att_list, string attname)
{
	vector<Tango::Attr *>::iterator it;
	for (it=att_list.begin() ; it<att_list.end() ; ++it)
		if ((*it)->get_name()==attname)
			return (*it);
	//	Attr does not exist
	return NULL;
}


/*----- PROTECTED REGION ID(TextToSpeechClass::Additional Methods) ENABLED START -----*/

/*----- PROTECTED REGION END -----*/	//	TextToSpeechClass::Additional Methods
} //	namespace
