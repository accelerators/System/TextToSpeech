# Functions and Pre-build  -----------------------------------

# Stop messy in source builds
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)
set(CMAKE_DISABLE_SOURCE_CHANGES  ON)

if ( ${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR} )
    message( FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there. You may need to remove CMakeCache.txt." )
endif()

# Start Build Config -----------------------------------
cmake_minimum_required (VERSION 3.1.0)
set(CMAKE_SKIP_RPATH true)
set(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_COLOR_MAKEFILE ON)

# Project definitions
project(TextToSpeech)

# Include the version strings
include(cmake/ReleaseVersion.cmake)

# Sub module options
option(TTS_ENABLE_TRACING "Enable tracing on all function calls" OFF)
option(TTS_ENABLE_DEBUG "Enable debugging output" ON)
option(TTS_UNIT_TEST_ENABLE_TRACING "Enable tracing on all function calls" OFF)
option(TTS_UNIT_TEST_ENABLE_DEBUG "Enable debugging output" OFF)

# Options
option(TEXT_TO_SPEECH_BUILD_TESTS "Build unit tests" ON)
option(TEXT_TO_SPEECH_BUILD_DEBUG_SYMBOLS "Build with debug symbol" OFF)
option(TEXT_TO_SPEECH_LOG_TO_TANGO "Redirect the tts library logging to Tango" ON)

if(TEXT_TO_SPEECH_BUILD_TESTS)
    message(STATUS "Unit tests will be built")
endif(TEXT_TO_SPEECH_BUILD_TESTS)

if(TEXT_TO_SPEECH_BUILD_DEBUG_SYMBOLS)
    message(STATUS "Building all with debug symbols")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g")
endif(TEXT_TO_SPEECH_BUILD_DEBUG_SYMBOLS)

if(TEXT_TO_SPEECH_LOG_TO_TANGO)
    message(STATUS "Enable logging redirect to tango logging system")
endif(TEXT_TO_SPEECH_LOG_TO_TANGO)

# set up a configuration file to pass variables into the build
configure_file(
    "${PROJECT_SOURCE_DIR}/cmake/TextToSpeechConfig.h.in"
    "${PROJECT_BINARY_DIR}/TextToSpeechConfig.h")

# Setup the source
set(SRC_FILES 
    TextToSpeech.cpp 
    TextToSpeechClass.cpp 
    TextToSpeechStateMachine.cpp
    ClassFactory.cpp 
    main.cpp)

# Add any include paths from the command line
list(APPEND INCLUDE_PATHS ${CMAKE_INCLUDE_PATH})
list(APPEND INCLUDE_PATHS ${CMAKE_SOURCE_DIR})
list(APPEND LIBRARY_PATHS ${CMAKE_LIBRARY_PATH})

# Start collecting the libraries this server depends on together
list(APPEND TEXT_TO_SPEECH_LIBRARIES dl pthread)

message(STATUS "Search for TANGO package config...")

# allow pkgconfig to search the CMAKE_PREFIX_PATH 
set(PKG_CONFIG_USE_CMAKE_PREFIX_PATH ON)
list(APPEND CMAKE_PREFIX_PATH "/usr")

find_package(PkgConfig REQUIRED)
pkg_search_module(TANGO REQUIRED tango>=9.2.5)

if(TANGO_FOUND)
    message(STATUS "Found tango version ${TANGO_VERSION} at ${TANGO_PREFIX}")
endif(TANGO_FOUND)

list(APPEND TEXT_TO_SPEECH_LIBRARIES ${TANGO_LIBRARIES})
list(APPEND TEXT_TO_SPEECH_INCLUDE_PATHS ${TANGO_INCLUDE_DIRS})
list(APPEND TEXT_TO_SPEECH_LIBRARY_PATHS ${TANGO_LIBDIR})
list(APPEND TEXT_TO_SPEECH_CFLAGS ${TANGO_CFLAGS_OTHER})
list(APPEND TEXT_TO_SPEECH_LDFLAGS ${TANGO_LDFLAGS_OTHER})

message(STATUS "Searching for libraries...")

# Now load the libraries and check each is found, if not
# this is an error condition, so halt the cmake config
foreach(LIB ${TEXT_TO_SPEECH_LIBRARIES})
    # try the user provided paths first
    find_library(FOUND_LIB_${LIB} ${LIB} PATHS ${TEXT_TO_SPEECH_LIBRARY_PATHS} NO_DEFAULT_PATH)

    # if we could not find it, drop to the system paths
    if (NOT FOUND_LIB_${LIB})
        find_library(FOUND_LIB_${LIB} ${LIB})
    endif()

    list(APPEND TEXT_TO_SPEECH_FOUND_LIBS ${FOUND_LIB_${LIB}})

    if (FOUND_LIB_${LIB})
        message(STATUS "Found " ${LIB} " at: " ${FOUND_LIB_${LIB}})
    else()
        message(FATAL "Could not find " ${LIB})
    endif()
endforeach(LIB)

if(TTS_UNIT_TEST_ENABLE_TRACING)
    message(STATUS "Enabled TTS_ENABLE_TRACING due to TTS_UNIT_TEST_ENABLE_TRACING")
    set(TTS_ENABLE_TRACING ON)
endif(TTS_UNIT_TEST_ENABLE_TRACING)

if(TTS_UNIT_TEST_ENABLE_DEBUG)
    message(STATUS "Enabled TTS_ENABLE_DEBUG due to TTS_UNIT_TEST_ENABLE_DEBUG")
    set(TTS_ENABLE_DEBUG ON)
endif(TTS_UNIT_TEST_ENABLE_DEBUG)

# Include the static tts backend library, this is a static library that will
# be linked against the device server
add_subdirectory(tts_library)

# Build Targets  -----------------------------------
add_executable(TextToSpeech ${SRC_FILES})
target_compile_options(TextToSpeech PRIVATE -std=c++14 -Wall -Wextra ${TEXT_TO_SPEECH_CFLAGS})

target_link_libraries(TextToSpeech 
    PRIVATE ${TEXT_TO_SPEECH_FOUND_LIBS} tts_static_library ${TEXT_TO_SPEECH_LDFLAGS})

target_include_directories(TextToSpeech 
    PRIVATE ${TEXT_TO_SPEECH_INCLUDE_PATHS} ${CMAKE_CURRENT_LIST_DIR} "${PROJECT_BINARY_DIR}")

if(TEXT_TO_SPEECH_LOG_TO_TANGO)
    target_compile_definitions(TextToSpeech PRIVATE -DREDIRECT_LOG_TO_TANGO)
endif(TEXT_TO_SPEECH_LOG_TO_TANGO)

# Assets -----------------------------------

# Prepare the audio files for use, copy to the build env and convert
set(JINGLE_DIR "${CMAKE_CURRENT_BINARY_DIR}/jingles")

add_custom_command(
    OUTPUT ${JINGLE_DIR}
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/jingles ${CMAKE_CURRENT_BINARY_DIR}/jingles)

add_custom_target(
    convert-audio 
    COMMAND ${CMAKE_SOURCE_DIR}/scripts/convert_audio_files "${CMAKE_CURRENT_BINARY_DIR}/jingles"
    DEPENDS ${JINGLE_DIR})

# Tests -----------------------------------
if(TEXT_TO_SPEECH_BUILD_TESTS)
    add_subdirectory(test)
endif(TEXT_TO_SPEECH_BUILD_TESTS)