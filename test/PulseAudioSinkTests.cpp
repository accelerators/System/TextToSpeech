// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#include "PulseAudioSink.hpp"
#include "JingleMgr.hpp" 
#include "TestUtils.hpp"
#include "catch.hpp"
#include <future>
#include <iostream>

using namespace std;
using namespace TTSLibrary;
using namespace Catch;

// Tests here are rather simple, this is a wrapper class that wraps
// up access to pulse audio into something helpful for the tts library

SCENARIO("Initialise/Destroy PulseAudioSink", "[pulseaudio]" )
{
    GIVEN("A PulseAudioSink")
    {
        PulseAudioSink sink;

        WHEN("Calling initialise()")
        {
            THEN("No exception should be thrown")
            {
                REQUIRE_NOTHROW(sink.initialise());
            }
            AND_WHEN("destroy() is called on an initialised sink")
            {
                REQUIRE_NOTHROW(sink.destroy());
            }
        }
    }
}

SCENARIO("Playing some test files", "[pulseaudio]" )
{
    GIVEN("A PulseAudioSink and a JingleMgr to load the file")
    {
        PulseAudioSink sink;
        JingleMgr mgr(JingleLocation);

        REQUIRE_NOTHROW(sink.initialise());
        REQUIRE(mgr.availableJingles().size() > 0);
        REQUIRE(mgr.jingleExists("notify.pcm"));

        WHEN("Calling playBuffer() with a valid buffer from the JingleMgr")
        {
            SharedDataBuffer buffer = mgr.getAudioBuffer("notify.pcm");
            REQUIRE(buffer->size() > 0);

            THEN("No exception should be thrown")
            {
                REQUIRE_NOTHROW(sink.playBuffer(buffer));
            }
        }
        AND_WHEN("Calling playBuffer() with an invalid buffer")
        {
            SharedDataBuffer buffer;

            THEN("playBuffer() should throw an exception")
            {
                REQUIRE_THROWS(sink.playBuffer(buffer));
            }
        }
    }
}

SCENARIO("Interupting playBuffer with stopPlaying", "[pulseaudio]" )
{
    GIVEN("A PulseAudioSink playing a valid file")
    {
        PulseAudioSink sink;
        JingleMgr mgr(JingleLocation);

        REQUIRE_NOTHROW(sink.initialise());
        REQUIRE(mgr.availableJingles().size() > 0);
        REQUIRE(mgr.jingleExists("notify.pcm"));

        SharedDataBuffer buffer = mgr.getAudioBuffer("notify.pcm");
        REQUIRE(buffer->size() > 0);

        WHEN("Interupting with stopPlayback()")
        {
            THEN("No exceptions raised")
            {
                auto playback = async(std::launch::async, 
                    [&buffer, &sink]()
                    {
                        REQUIRE_NOTHROW(sink.playBuffer(buffer));
                        return true;                    
                    });
                    
                std::this_thread::sleep_for(std::chrono::seconds(2));
                REQUIRE_NOTHROW(sink.stopPlayback());

                REQUIRE(playback.get() == true);                
                REQUIRE_NOTHROW(sink.destroy());
            }
        }
    }
    GIVEN("A PulseAudioSink not playing any files")
    {
        PulseAudioSink sink;
        REQUIRE_NOTHROW(sink.initialise());

        WHEN("Calling stopPlayback()")
        {
            THEN("stopPlayback() should not throw an exception")
            {
                REQUIRE_NOTHROW(sink.stopPlayback());              
            }            
        }
    }
}