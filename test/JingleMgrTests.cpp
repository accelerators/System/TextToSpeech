// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#include "JingleMgr.hpp"
#include "TestUtils.hpp"
#include "catch.hpp"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;
using namespace std;
using namespace TTSLibrary;
using namespace Catch;

SCENARIO("Load jingle directory and index files", "[jinglemgr][fs]" )
{
    GIVEN("An invalid directory")
    {
        string dir = "invalid";

        WHEN("Creating a JingleMgr")
        {
            THEN("The invalid directory causes an exception")
            {
                REQUIRE_THROWS(JingleMgr(dir));
            }
        }
    }
    GIVEN("An empty valid directory")
    {
        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);

        fs::create_directory(TestJingleLocation);

        WHEN("Creating a JingleMgr")
        {
            THEN("No exception is thrown")
            {
                REQUIRE_NOTHROW(JingleMgr(TestJingleLocation));
            }
        }

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);        
    }
    GIVEN("The local directory")
    {
        string dir = ".";

        WHEN("Creating a JingleMgr")
        {
            THEN("An exception is thrown")
            {
                REQUIRE_THROWS(JingleMgr(dir));
            }
        }        
    }
    GIVEN("An empty string as the directory")
    {
        WHEN("Creating a JingleMgr")
        {
            THEN("An exception is thrown")
            {
                REQUIRE_THROWS(JingleMgr(""));
            }
        }        
    }  
    GIVEN("An valid directory with 3 test files")
    {
        vector<string> files = { "file1.pcm", "file2.pcm", "file3.pcm" };

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);

        fs::create_directory(TestJingleLocation);

        for(auto name : files)
        {
            fstream file;
            file.open(TestJingleLocation + "/" + name, ios::out);
            file << "000000000000000000000000000000000000000";
            file.close();
        }

        WHEN("Creating a JingleMgr")
        {
            JingleMgr jingle_mgr(TestJingleLocation);

            THEN("No exception is thrown")
            {
                REQUIRE_NOTHROW(JingleMgr(TestJingleLocation));
            }
            AND_THEN("Loaded Jingles is 3")
            {
                REQUIRE(jingle_mgr.availableJingles().size() == 3);
            }
        }
        
        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);        
    }    
}

SCENARIO("Load jingle directory and verify files", "[jinglemgr][fs]" )
{
    GIVEN("An valid directory with 3 test files")
    {
        vector<string> files = { "file1.pcm", "file2.pcm", "file3.pcm" };

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);

        fs::create_directory(TestJingleLocation);

        for(auto name : files)
        {
            fstream file;
            file.open(TestJingleLocation + "/" + name, ios::out);
            file << "000000000000000000000000000000000000000";
            file.close();
        }

        WHEN("Creating a JingleMgr")
        {
            JingleMgr jingle_mgr(TestJingleLocation);

            THEN("Each file exists when calling jingleExists()")
            {
                for_each(files.begin(), files.end(), 
                    [&jingle_mgr](string file) { REQUIRE(jingle_mgr.jingleExists(file)); });
            }
        }

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);        
    }    
}

SCENARIO("Load jingle into a buffer", "[jinglemgr][fs]" )
{
    GIVEN("A JingleMgr with 3 jingles")
    {
        string data = "123456789";
        vector<string> files = { "file1.pcm", "file2.pcm", "file3.pcm" };

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);

        fs::create_directory(TestJingleLocation);

        for(auto name : files)
        {
            fstream file;
            file.open(TestJingleLocation + "/" + name, ios::out);
            file << data;
            file.close();
        }

        JingleMgr jingle_mgr(TestJingleLocation);
        REQUIRE(jingle_mgr.availableJingles().size() == 3);

        WHEN("Requesting a jingle name as a buffer")
        {
            SharedDataBuffer buffer;
            REQUIRE_NOTHROW(buffer = jingle_mgr.getAudioBuffer("file1.pcm"));

            THEN("Returned buffer size and contents is valid")
            {
                REQUIRE(buffer->size() == 9);
                REQUIRE(string(buffer->begin(), buffer->end()) == "123456789");
            }
        }
        AND_WHEN("Request an invalid jingle name as a buffer")
        {
            THEN("An exception is thrown")
            {
                REQUIRE_THROWS(jingle_mgr.getAudioBuffer("invalid.pcm"));
            }
        }
        AND_WHEN("Request an invalid jingle name as a buffer")
        {
            fstream file;
            file.open(TestJingleLocation + "/" + "zero.pcm", ios::out);
            file.close();
            REQUIRE(jingle_mgr.availableJingles().size() == 4); 

            THEN("An exception is thrown")
            {
                REQUIRE_THROWS(jingle_mgr.getAudioBuffer("zero.pcm"));
            }
        }    

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);
    }    
}

SCENARIO("Updating search directory effects caching", "[jinglemgr][fs]" )
{
    GIVEN("A JingleMgr with 3 jingles")
    {
        string data = "123456789";
        vector<string> files = { "file1.pcm", "file2.pcm", "file3.pcm" };

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);

        fs::create_directory(TestJingleLocation);

        for(auto name : files)
        {
            fstream file;
            file.open(TestJingleLocation + "/" + name, ios::out);
            file << data;
            file.close();
        }

        JingleMgr jingle_mgr(TestJingleLocation);
        REQUIRE(jingle_mgr.availableJingles().size() == 3);

        WHEN("Requesting a jingle name as a buffer")
        {
            SharedDataBuffer buffer;
            REQUIRE_NOTHROW(buffer = jingle_mgr.getAudioBuffer("file1.pcm"));

            THEN("Jingle shows as cached")
            {
                REQUIRE(jingle_mgr.jingleCached() == "file1.pcm");
            }
        }
        AND_WHEN("The search directory is changed")
        {
            string dir2 = "jingle_test_two";

            if(fs::exists(dir2))
                fs::remove_all(dir2);

            fs::create_directory(dir2);

            jingle_mgr.updateSearchDirectory(dir2);

            THEN("Cached name is now empty")
            {
                REQUIRE(jingle_mgr.jingleCached().empty() == true);
            }

            if(fs::exists(dir2))
                fs::remove_all(dir2);            
        }

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);
    }    
}