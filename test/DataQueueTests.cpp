// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#include "DataQueue.hpp"
#include "catch.hpp"
#include <future>

using namespace std;
using namespace TTSLibrary;
using namespace Catch;

SCENARIO("Initialisation state", "[dataqueue][utils]" )
{
    GIVEN("A DataQueue<T> object, where T is int")
    {
        DataQueue<int> queue;

        WHEN("No operations carried out")
        {
            THEN("The queue is size zero and empty")
            {
                REQUIRE(queue.size() == 0);
                REQUIRE(queue.empty() == true);
            }
        }
    }
}

SCENARIO("Queue Manipulation", "[dataqueue][utils]" )
{
    GIVEN("A DataQueue<T> object, where T is int")
    {
        DataQueue<int> queue;

        WHEN("Pushing an item (1) onto the queue")
        {
            queue.push(1);

            THEN("Queue grows by size 1 and is not empty")
            {
                REQUIRE(queue.size() == 1);
                REQUIRE(queue.empty() == false);
            }
            AND_WHEN("A second item (2) is pushed onto the queue")
            {
                queue.push(2);

                THEN("Queue grows by size 1 and is size 2")
                {
                    REQUIRE(queue.size() == 2);
                    REQUIRE(queue.empty() == false);
                }
                AND_WHEN("The queue pop is called")
                {
                    int i;
                    queue.pop(i);

                    THEN("Queue shrinks by size 1 and is size 1")
                    {
                        REQUIRE(queue.size() == 1);
                        REQUIRE(queue.empty() == false);
                    }
                    AND_WHEN("The queue pop is called again")
                    {
                        int i;
                        queue.pop(i);

                        THEN("Queue shrinks by size 1, is size 0 and empty")
                        {
                            REQUIRE(queue.size() == 0);
                            REQUIRE(queue.empty() == true);
                        }
                    }                                           
                }                                
            }                  
        }
    }
}

SCENARIO("Queue Item Verification", "[dataqueue][utils]" )
{
    GIVEN("A DataQueue<T> object, where T is int")
    {
        DataQueue<int> queue;

        WHEN("Pushing an two items (5, 9) onto the queue in given order (5, 9)")
        {
            queue.push(5);
            queue.push(9);

            THEN("Queue grows by size 2 and is not empty")
            {
                REQUIRE(queue.size() == 2);
                REQUIRE(queue.empty() == false);
            }
            AND_WHEN("Pop an item from the queue")
            {
                int i;
                queue.pop(i);

                THEN("We expect the first item (5) and queue to shrink")
                {
                    REQUIRE(i == 5);
                    REQUIRE(queue.size() == 1);
                }
                AND_WHEN("Pop a second item from the queue")
                {
                    int i;
                    queue.pop(i);

                    THEN("We expect the second item (9) and queue to be empty")
                    {
                        REQUIRE(i == 9);
                        REQUIRE(queue.empty() == true);
                    }
                }                     
            }                
        }
    }
}

SCENARIO("Queue Clears Correctly", "[dataqueue][utils]" )
{
    GIVEN("A DataQueue<T> object, where T is int")
    {
        DataQueue<int> queue;

        WHEN("Pushing on two items (5, 9) onto the queue in given order (5, 9)")
        {
            queue.push(5);
            queue.push(9);

            THEN("Queue grows by size 2 and is not empty")
            {
                REQUIRE(queue.size() == 2);
                REQUIRE(queue.empty() == false);
            }
            AND_WHEN("The queue is cleared")
            {
                queue.clear();

                THEN("We expect the queue to be empty")
                {
                    REQUIRE(queue.size() == 0);
                    REQUIRE(queue.empty() == true);
                }                    
            }                
        }
    }
}

struct TestItem
{
    TestItem() = default;
    TestItem(const TestItem&) = delete;
    TestItem(TestItem &&) { }
    TestItem& operator=(TestItem &&) { return *this; }
};

SCENARIO("Queue Push(&&) does a move, not a copy", "[dataqueue][utils]" )
{
    GIVEN("A DataQueue<T> object, where T is int")
    {
        DataQueue<TestItem> queue;

        WHEN("Pushing on two items (5, 9) onto the queue in given order (5, 9)")
        {
            queue.push(TestItem());
            queue.push(TestItem());

            THEN("Queue grows by size 2 and is not empty")
            {
                REQUIRE(queue.size() == 2);
                REQUIRE(queue.empty() == false);
            }
        }
    }
}

SCENARIO("Queue Pop(&) acts accordingly based on overloaded call", "[dataqueue][utils]" )
{
    GIVEN("A DataQueue<T> object, where T is int")
    {
        DataQueue<int> queue;

        WHEN("The queue is empty")
        {
            THEN("Calling Pop with a timeout, times out")
            {
                int i;
                REQUIRE(queue.pop(i, 10) == false);
            }
            AND_THEN("Calling Pop without a timeout requires a push()")
            {
                auto pop = async(std::launch::async, 
                    [&queue]()
                    {
                        int i;
                        REQUIRE(queue.pop(i) == true);
                    });

                auto push = async(std::launch::deferred, 
                    [&queue]()
                    {
                        int i = 6;
                        queue.push(i);
                    });

                push.get();
                pop.get();
            }
        }
    }
}