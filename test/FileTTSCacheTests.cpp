// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#include "FileTTSCache.hpp"
#include "TestUtils.hpp"
#include "catch.hpp"

#include <fstream>
#include <experimental/filesystem>

using namespace std;
namespace fs = std::experimental::filesystem;
using namespace TTSLibrary;
using namespace Catch;

SCENARIO("Constructing a File Cache", "[cache][file-cache][fs]" )
{
    GIVEN("A new directory to use as a cache location")
    {
        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);

        WHEN("Directory does not exist and create enable")
        {
            THEN("A new directory is created")
            {
                REQUIRE(fs::exists(TestCacheLocation) == false);
                REQUIRE_NOTHROW(FileTTSCache(TestCacheLocation, true));
                REQUIRE(fs::is_directory(TestCacheLocation) == true);
            }
        }
        WHEN("Directory does not exist and create disabled")
        {
            THEN("An exception is raised")
            {
                REQUIRE(fs::exists(TestCacheLocation) == false);
                REQUIRE_THROWS(FileTTSCache(TestCacheLocation, false));
            }
        }
        WHEN("Directory exists and create enabled")
        {
            THEN("Current directory is used")
            {
                CHECK(fs::create_directory(TestCacheLocation) == true);
                REQUIRE(fs::exists(TestCacheLocation) == true);
                REQUIRE_NOTHROW(FileTTSCache(TestCacheLocation, true));
                REQUIRE(fs::is_directory(TestCacheLocation) == true);
            }            
        }
        WHEN("Directory exists and is an invalid object")
        {
            THEN("An exception is raised")
            {
                fstream file(TestCacheLocation, fstream::out);
                file << "Test";
                file.close();

                REQUIRE(fs::exists(TestCacheLocation) == true);
                REQUIRE_THROWS(FileTTSCache(TestCacheLocation, true));
            }         
        }

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);       
    }
    GIVEN("The local directory")
    {
        string dir = ".";

        WHEN("Creating a FileTTSCache")
        {
            THEN("An exception is thrown")
            {
                REQUIRE_THROWS(FileTTSCache(dir, true));
            }
        }        
    }
    GIVEN("An empty string as the directory")
    {
        WHEN("Creating a FileTTSCache")
        {
            THEN("An exception is thrown")
            {
                REQUIRE_THROWS(FileTTSCache("", true));
            }
        }        
    }
}

SCENARIO("Adding to the cache", "[cache][file-cache][fs]" )
{
    GIVEN("An empty file cache object")
    {
        string voice_name1 = "henry";

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);

        FileTTSCache *cache = nullptr;
        REQUIRE_NOTHROW(cache = new FileTTSCache(TestCacheLocation, true));
        REQUIRE(fs::exists(TestCacheLocation) == true);

        WHEN("An item of text is added to the cache")
        {
            string cache_text = "Test Text";
            DataBuffer vector_buffer = {'s', 'o', 'm', 'e', 't', 'e', 'x', 't'};
            SharedDataBuffer buffer = make_shared<const DataBuffer>(vector_buffer);

            string name;
            REQUIRE_NOTHROW(name = cache->cache(cache_text, buffer, voice_name1));

            THEN("The cache size increases by 1 and the file exists on disk")
            {
                REQUIRE(cache->cacheSize() == 1);
                REQUIRE(fs::exists(name));
                REQUIRE(cache->isCached(cache_text, voice_name1));

                AND_THEN("The file is opened and read")
                {
                    fstream file(name, ios::in);
                    REQUIRE(file.good());
                    stringstream in_ss;
                    in_ss << file.rdbuf();
                    file.close();

                    REQUIRE(string(buffer->begin(), buffer->end()) == in_ss.str());
                }
                AND_WHEN("The same item of text is added again")
                {
                    THEN("The cache throws and exception")
                    {
                        REQUIRE_THROWS(cache->cache(cache_text, buffer, voice_name1));
                    }
                }
                AND_WHEN("A second item of text is added to the cache")
                {
                    string cache_text = "Test Text Second";
                    string name;
                    REQUIRE_NOTHROW(name = cache->cache(cache_text, buffer, voice_name1));
        
                    THEN("The cache size increases by 1 to 2 and the file exists on disk")
                    {
                        REQUIRE(cache->cacheSize() == 2);
                        REQUIRE(fs::exists(name));
                        REQUIRE(cache->isCached(cache_text, voice_name1));

                        AND_THEN("The file is opened and read")
                        {
                            fstream file(name, ios::in);
                            REQUIRE(file.good());
                            stringstream in_ss;
                            in_ss << file.rdbuf();
                            file.close();

                            REQUIRE(string(buffer->begin(), buffer->end()) == in_ss.str());
                        }
                    }                    
                }                
            }
        }
        WHEN("An an empty buffer is added to the cache")
        {
            string cache_text = "Test Text";
            DataBuffer vector_buffer;
            SharedDataBuffer buffer = make_shared<const DataBuffer>(vector_buffer);

            THEN("The cache throws an exception")
            {
                REQUIRE_THROWS(cache->cache(cache_text, buffer, voice_name1));
            }
        }
        WHEN("An an null buffer is added to the cache")
        {
            string cache_text = "Test Text";
            SharedDataBuffer buffer;

            THEN("The cache throws an exception")
            {
                REQUIRE_THROWS(cache->cache(cache_text, buffer, voice_name1));
            }
        }

        delete cache;

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);        
    }
}

SCENARIO("Retriving an item from the cache", "[cache][file-cache][fs]" )
{
    GIVEN("An file cache object with 1 item in it")
    {
        string voice_name1 = "henry";

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);

        FileTTSCache *cache = nullptr;
        REQUIRE_NOTHROW(cache = new FileTTSCache(TestCacheLocation, true));
        REQUIRE(fs::exists(TestCacheLocation) == true);

        string cache_text = "Test Text";
        DataBuffer vector_buffer = {'s', 'o', 'm', 'e', 't', 'e', 'x', 't'};
        SharedDataBuffer buffer = make_shared<const DataBuffer>(vector_buffer);

        string name;
        REQUIRE_NOTHROW(name = cache->cache(cache_text, buffer, voice_name1));
        REQUIRE(fs::exists(name) == true);

        WHEN("The item is requested from the cache")
        {
            SharedDataBuffer ret_buffer;
            REQUIRE_NOTHROW(ret_buffer = cache->retrieveCacheEntry(cache_text, voice_name1));

            THEN("The cache returns a buffer with the cache item loaded")
            {
                REQUIRE(ret_buffer != nullptr);
                REQUIRE(ret_buffer->size() != 0);
                REQUIRE(string(buffer->begin(), buffer->end()) == string(ret_buffer->begin(), ret_buffer->end()));
            }
        }

        delete cache;

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);        
    }
}

SCENARIO("Removing an item from the cache", "[cache][file-cache][fs]" )
{
    GIVEN("An file cache object with 1 item in it")
    {
        string voice_name1 = "henry";

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);

        FileTTSCache *cache = nullptr;
        REQUIRE_NOTHROW(cache = new FileTTSCache(TestCacheLocation, true));
        REQUIRE(fs::exists(TestCacheLocation) == true);

        string cache_text = "Test Text";
        DataBuffer vector_buffer = {'s', 'o', 'm', 'e', 't', 'e', 'x', 't'};
        SharedDataBuffer buffer = make_shared<const DataBuffer>(vector_buffer);

        string name;
        REQUIRE_NOTHROW(name = cache->cache(cache_text, buffer, voice_name1));
        REQUIRE(cache->cacheSize() == 1);
        REQUIRE(cache->isCached(cache_text, voice_name1));

        WHEN("The item is removed from the cache")
        {
            cache->removeCacheEntry(cache_text, voice_name1);

            THEN("The cache size decreases by 1 and the file does not exist on disk")
            {
                REQUIRE(cache->cacheSize() == 0);
                REQUIRE(cache->isCached(cache_text, voice_name1) == false);
                REQUIRE(fs::exists(name) == false);

                AND_WHEN("Same item is removed again")
                {
                    THEN("An exception is thrown")
                    {
                        REQUIRE_THROWS(cache->removeCacheEntry(cache_text, voice_name1));
                    }
                }
            }
        }

        delete cache;

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);        
    }
}

SCENARIO("Caching for multiple voices", "[cache][file-cache][fs]" )
{
    GIVEN("An file cache object")
    {
        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);

        FileTTSCache *cache = nullptr;
        REQUIRE_NOTHROW(cache = new FileTTSCache(TestCacheLocation, true));
        REQUIRE(fs::exists(TestCacheLocation) == true);

        string cache_text = "Test Text";
        DataBuffer vector_buffer = {'s', 'o', 'm', 'e', 't', 'e', 'x', 't'};
        SharedDataBuffer buffer = make_shared<const DataBuffer>(vector_buffer);

        WHEN("The item is cached with voice name 1")
        {
            string voice_name1 = "henry";
            string name;
            REQUIRE_NOTHROW(name = cache->cache(cache_text, buffer, voice_name1));

            THEN("Item exists in the cache and on disk")
            {
                REQUIRE(cache->cacheSize() == 1);
                REQUIRE(cache->isCached(cache_text, voice_name1));
                REQUIRE(fs::exists(name));

                AND_WHEN("A second item with same text and a different name is cached")
                {
                    string voice_name2 = "bert";
                    string name;
                    REQUIRE_NOTHROW(name = cache->cache(cache_text, buffer, voice_name2));

                    THEN("That item exists in the cache and on disk")
                    {
                        REQUIRE(cache->cacheSize() == 2);
                        REQUIRE(cache->isCached(cache_text, voice_name2));
                        REQUIRE(fs::exists(name));
                    }
                }
            }
        }

        delete cache;

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);        
    }
}