// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#define CATCH_CONFIG_RUNNER

#include "catch.hpp"
#include "Log.h"

int main(int argc, char *argv[]) 
{
    #ifdef DEBUG_ENABLED
        LoggingUtils::LoggerClass::Log::LogLevel() = LoggingUtils::Debug;
    #else
        LoggingUtils::LoggerClass::Log::LogLevel() = LoggingUtils::Disabled;
    #endif

    int result = Catch::Session().run(argc, argv);
    return result;
}