// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#include "PollyTTSDriver.hpp"
#include "TTSUtils.hpp"
#include "TestUtils.hpp"
#include "catch.hpp"
#include <cstdlib>

using namespace std;
using namespace TTSLibrary;
using namespace Catch;

SCENARIO("Initalise PollyTTSDriver", "[pollytts][connected]" )
{
    GIVEN("A PollyTTSDriver object")
    {
        REQUIRE(getenv("AWS_ACCESS_KEY_ID"));
        REQUIRE(getenv("AWS_SECRET_ACCESS_KEY"));

        pair<string, int> proxy_settings = get_proxy_settings();

        PollyTTSDriver tts_driver;

        WHEN("initialise() called with valid default proxy")
        {
            THEN("No exception is thrown")
            {
                REQUIRE_NOTHROW(tts_driver.initialise(proxy_settings.first, proxy_settings. second));
            }
        }     
    }
}

SCENARIO("Request available voice", "[pollytts][connected]" )
{
    GIVEN("Correctly configured PollyDriver and environment")
    {
        REQUIRE(getenv("AWS_ACCESS_KEY_ID"));
        REQUIRE(getenv("AWS_SECRET_ACCESS_KEY"));

        pair<string, int> proxy_settings = get_proxy_settings();

        PollyTTSDriver tts_driver;
        tts_driver.initialise(proxy_settings.first, proxy_settings. second);

        WHEN("Requesting available voices")
        {
            vector<string> voices = tts_driver.availableVoices();

            THEN("A list of English speacking voices is returned")
            {
                REQUIRE(voices.size() > 0);
            }
        }
    }
}

SCENARIO("Selecting voice", "[pollytts][connected]" )
{
    GIVEN("Correctly configured PollyDriver and environment")
    {
        REQUIRE(getenv("AWS_ACCESS_KEY_ID"));
        REQUIRE(getenv("AWS_SECRET_ACCESS_KEY"));

        pair<string, int> proxy_settings = get_proxy_settings();

        PollyTTSDriver tts_driver;
        tts_driver.initialise(proxy_settings.first, proxy_settings. second);

        WHEN("Requesting available voice")
        {
            THEN("Get a vector of voices with a size larger than 0")
            {
                vector<string> voices = tts_driver.availableVoices();
                REQUIRE(voices.size() > 0);
            }
        }
    }
}

SCENARIO("Requesting speech", "[pollytts][connected]" )
{
    GIVEN("Correctly configured PollyDriver and environment")
    {
        REQUIRE(getenv("AWS_ACCESS_KEY_ID"));
        REQUIRE(getenv("AWS_SECRET_ACCESS_KEY"));

        pair<string, int> proxy_settings = get_proxy_settings();

        PollyTTSDriver tts_driver;
        tts_driver.initialise(proxy_settings.first, proxy_settings. second);        

        WHEN("Requesting a speech buffer for text \"hello\"")
        {
            vector<string> voices = tts_driver.availableVoices();
            REQUIRE(voices.size() > 0);
            SharedDataBuffer buffer;
            
            REQUIRE_NOTHROW(buffer = tts_driver.getSpeechBuffer("hello", voices[0]));

            THEN("The buffer size is greater than 0")
            {
                REQUIRE(buffer->size() > 0);
                REQUIRE(buffer.use_count() == 1);
            }
            AND_WHEN("A second request is sent")
            {
                REQUIRE_NOTHROW(buffer = tts_driver.getSpeechBuffer("another test", voices[0]));

                THEN("The buffer size is greater than 0")
                {
                    REQUIRE(buffer->size() > 0);
                    REQUIRE(buffer.use_count() == 1);
                }
            }
            AND_WHEN("An invalid voice is requested")
            {
                THEN("An exception is raised")
                {
                    REQUIRE_THROWS(tts_driver.getSpeechBuffer("Invalid test", "Invalid voice"));
                }
            }
        }
    }
}