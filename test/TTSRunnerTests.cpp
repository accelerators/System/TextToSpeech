// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#include "TTSRunner.hpp"
#include "TTSUtils.hpp"
#include "TestUtils.hpp"
#include "catch.hpp"
#include <algorithm>
#include <fstream>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;
using namespace std;
using namespace TTSLibrary;
using namespace Catch;

// some general data for testing
const string FallbackMessage = "Fallback Message";

// Difficult class to test. We need to use some mocks and dependency injection 
// really, but it would break the pattern used, that the runner is encapsulated 

SCENARIO("Initialisation and destroying", "[ttsrunner]" )
{
    GIVEN("A valid object to initialise")
    {
        TTSRunner runner;
        
        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);

        fs::create_directory(TestJingleLocation);
        fs::create_directory(TestCacheLocation);

        pair<string, int> proxy_settings = get_proxy_settings();

        WHEN("Initialising with valid cache and jingle location")
        {
            THEN("No exception is thrown")
            {
                REQUIRE_NOTHROW(runner.initialise(proxy_settings.first, proxy_settings. second));
                REQUIRE_NOTHROW(runner.initialiseCache(TestCacheLocation, ""));
                REQUIRE_NOTHROW(runner.initialiseJingle(TestJingleLocation));
            }
            AND_WHEN("The TTSRunenr is destroyed")
            {
                THEN("No exception is thrown")
                {
                    REQUIRE_NOTHROW(runner.destroy());
                }
            }
        }

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);
    }
    GIVEN("An uninitalised TTSRunner")
    {
        TTSRunner runner;

        WHEN("Trying to call a function without initialisation")
        {
            THEN("An exception is thrown")
            {
                REQUIRE_THROWS(runner.clearAllRequests());
            }
        }        
    }
}

SCENARIO("Selecting and a jingle", "[ttsrunner]" )
{
    GIVEN("A TTSRunner setup with a valid directory and some jingles")
    {
        vector<string> files = { "file1.pcm", "file2.pcm", "file3.pcm" };

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);

        fs::create_directory(TestJingleLocation);

        for(auto name : files)
        {
            fstream file;
            file.open(TestJingleLocation + "/" + name, ios::out);
            file << "000000000000000000000000000000000000000";
            file.close();
        }

        pair<string, int> proxy_settings = get_proxy_settings();   

        TTSRunner runner;
        REQUIRE_NOTHROW(runner.initialise(proxy_settings.first, proxy_settings. second));
        REQUIRE_NOTHROW(runner.initialiseCache(TestCacheLocation, ""));
        REQUIRE_NOTHROW(runner.initialiseJingle(TestJingleLocation));

        WHEN("Requesting available voices")
        {
            vector<string> jingles = runner.availableJingles();

            THEN("A list of 3 jingles is returned")
            {
                REQUIRE(jingles.size() == 3);
            }
            AND_WHEN("We validate a jingle from the list and set it")
            {
                THEN("No exception is thrown, and the result is true")
                {
                    bool result;
                    REQUIRE_NOTHROW(result = runner.validateJingle(jingles[0]));
                    REQUIRE(result == true);
                }
            }
            AND_WHEN("We try validate an invalid jingle")
            {
                THEN("No exception is thrown, and the result is false")
                {
                    bool result;
                    REQUIRE_NOTHROW(result = runner.validateJingle("invalid"));
                    REQUIRE(result == false);
                }
            }
        }

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);
    }
}

SCENARIO("Selecting and setting a voice", "[ttsrunner][connected]" )
{
    GIVEN("A TTSRunner setup with a valid cache and jingle location")
    {
        REQUIRE(getenv("AWS_ACCESS_KEY_ID"));
        REQUIRE(getenv("AWS_SECRET_ACCESS_KEY"));

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);

        fs::create_directory(TestJingleLocation);
        fs::create_directory(TestCacheLocation);

        pair<string, int> proxy_settings = get_proxy_settings();

        TTSRunner runner;
        REQUIRE_NOTHROW(runner.initialise(proxy_settings.first, proxy_settings. second));
        REQUIRE_NOTHROW(runner.initialiseCache(TestCacheLocation, ""));
        REQUIRE_NOTHROW(runner.initialiseJingle(TestJingleLocation));

        WHEN("Requesting available voices")
        {
            vector<string> voices = runner.availableVoices();

            THEN("A list of voices returned")
            {
                REQUIRE(voices.size() > 0);
            }
            AND_WHEN("We validate a voice from the list")
            {
                THEN("No exception is thrown")
                {
                    bool result;
                    REQUIRE_NOTHROW(result = runner.validateVoice(voices[0]));
                    REQUIRE(result == true);
                }
            }
            AND_WHEN("We validate an invalid voice")
            {
                THEN("An exception is not thrown and the result is false")
                {
                    bool result;
                    REQUIRE_NOTHROW(result = runner.validateVoice("invalid"));
                    REQUIRE(result == false);                    
                }
            }
        }

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);        
    }
}

SCENARIO("Setting speech delay", "[ttsrunner][connected]" )
{
    GIVEN("A TTSRunner setup with a valid cache and jingle location")
    {
        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);

        fs::create_directory(TestJingleLocation);
        fs::create_directory(TestCacheLocation);

        pair<string, int> proxy_settings = get_proxy_settings();

        TTSRunner runner;
        REQUIRE_NOTHROW(runner.initialise(proxy_settings.first, proxy_settings. second));
        REQUIRE_NOTHROW(runner.initialiseCache(TestCacheLocation, ""));
        REQUIRE_NOTHROW(runner.initialiseJingle(TestJingleLocation));

        WHEN("Setting the speech delay to 50")
        {
            REQUIRE(runner.speechDelay(50) == true);

            THEN("Same value is returned")
            {
                REQUIRE(runner.speechDelay() == 50);
            }
            AND_WHEN("We set it to a value greater than 1001")
            {
                THEN("It returns false an no change is made")
                {
                    REQUIRE(runner.speechDelay(1001) == false);
                    REQUIRE(runner.speechDelay() == 50);                    
                }
            }
            AND_WHEN("We set it to a value to less than 0")
            {
                THEN("It returns false an no change is made")
                {
                    REQUIRE(runner.speechDelay(-1) == false);
                    REQUIRE(runner.speechDelay() == 50);                    
                }
            }            
        }

        if(fs::exists(TestJingleLocation))
            fs::remove_all(TestJingleLocation);

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);        
    }
}

SCENARIO("Fallback Audio File Creation", "[ttsrunner]" )
{
    GIVEN("A valid object to initialise")
    {
        REQUIRE(getenv("AWS_ACCESS_KEY_ID"));
        REQUIRE(getenv("AWS_SECRET_ACCESS_KEY"));

        TTSRunner runner;
        
        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);

        fs::create_directory(TestJingleLocation);
        fs::create_directory(TestCacheLocation);

        pair<string, int> proxy_settings = get_proxy_settings();

        WHEN("Initialising with valid cache")
        {
            REQUIRE_NOTHROW(runner.initialise(proxy_settings.first, proxy_settings. second));
            REQUIRE_NOTHROW(runner.initialiseCache(TestCacheLocation, FallbackMessage));

            THEN("Create audio files for the fallback message for each voice")
            {
                vector<string> voices = runner.availableVoices();

                for(auto voice : voices)
                {
                    REQUIRE(fs::exists(TestCacheLocation + "/" + voice) == true);

                    REQUIRE(count_if(
                        fs::directory_iterator(TestCacheLocation + "/" + voice),
                        fs::directory_iterator(),
                        [](const fs::directory_entry &entry){
                            return fs::is_regular_file(entry.path());
                        }) == 1);
                }
            }
        }

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);
    }
}

SCENARIO("Stats Increment On Use", "[ttsrunner]" )
{
    GIVEN("An initialised TTSRunner to start counting with")
    {
        REQUIRE(getenv("AWS_ACCESS_KEY_ID"));
        REQUIRE(getenv("AWS_SECRET_ACCESS_KEY"));

        TTSRunner runner;
        
        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);

        fs::create_directory(TestJingleLocation);
        fs::create_directory(TestCacheLocation);

        pair<string, int> proxy_settings = get_proxy_settings();

        REQUIRE_NOTHROW(runner.initialise(proxy_settings.first, proxy_settings. second));
        REQUIRE_NOTHROW(runner.initialiseCache(TestCacheLocation, FallbackMessage));
        runner.clearStats();
        
        TTSRunner::TTSRequest request;
        request._text = "Test";
        request._voice = runner.availableVoices()[0];

        WHEN("Checking initialise stats")
        {
            THEN("All should be equal to zero")
            {
                REQUIRE(runner.cacheHits() == 0);
                REQUIRE(runner.pollyRequests() == 0);
            }
            AND_WHEN("A new, uncached, request is made")
            {
                auto result = runner.addRequest(request);
                REQUIRE(result.get() == true);

                THEN("Polly requests increase by 1")
                {
                    REQUIRE(runner.cacheHits() == 0);
                    REQUIRE(runner.pollyRequests() == 1);
                }
                AND_WHEN("A request is made of cached speech")
                {
                    auto result = runner.addRequest(request);
                    REQUIRE(result.get() == true);

                    THEN("Cache hits increase by 1")
                    {
                        REQUIRE(runner.cacheHits() == 1);
                        REQUIRE(runner.pollyRequests() == 1);
                    }
                    AND_WHEN("We clear the stats")
                    {
                        runner.clearStats();

                        THEN("All stats return to zero")
                        {
                            REQUIRE(runner.cacheHits() == 0);
                            REQUIRE(runner.pollyRequests() == 0);
                        }
                    }                    
                }
            }
        }

        if(fs::exists(TestCacheLocation))
            fs::remove_all(TestCacheLocation);
    }
}