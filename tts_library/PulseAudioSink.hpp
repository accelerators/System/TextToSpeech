// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _PULSE_AUDIO_SINK_HPP
#define _PULSE_AUDIO_SINK_HPP

#include "TTSDataTypes.hpp"

#include <atomic>
#include <pulse/pulseaudio.h>

namespace TTSLibrary
{
// Designed to play buffers for the tts library module. This audio sink is
// not generic and is coded to suit the tt module playback. Internally it
// uses the thread pulse audio mainloop, with a number of callbacks to
// receieve events. Playing is a blocking call, since we needs to know
// when the audio buffer playback completes
class PulseAudioSink
{
public:
    // Constructor does very little, we defer init to the initialise
    // function below
    PulseAudioSink();
    virtual ~PulseAudioSink();

    // Initialise the sink, this will start the pulse audio thread, then
    // attempt to connect to pulse audio
    void initialise();

    // Stop the internal thread and deallocate the various resources
    // it allocate while running
    void destroy();

    // Stop playing the current audio buffer. Will attempt to cork
    // then flush the pulse audio stream
    void stopPlayback();

    // Manage the volume of the playback
    void volume(int volume);
    int volume();

    // Play the given audio buffer until completed. The function blocks
    // until the final bytes have been drained from the audio buffers.
    void playBuffer(SharedDataBuffer buffer);

private:
    // generic init check for accessor functions
    void initCheck();

    // pulse audio callbacks
    static void paContextStateCallback(pa_context *context, void *user_data);
    static void paStreamStateCallback(pa_stream *stream, void *user_data);
    static void paStreamWriteCallback(pa_stream *, size_t bytes, void *user_data);
    static void paStreamWriteFreeCallback(void *);
    static void paOperationSuccess(pa_stream *, int success, void *user_data);

    // internal pulse audio functions
    void paMainloopInit();
    void paMainloopFinish();
    void paConnectContext();
    void paConnectStream();
    pa_operation_state_t waitForOperation(pa_operation *operation);

    // pulse audio data
    pa_threaded_mainloop *_pa_main_loop;
    pa_sample_spec _sample_spec;
    pa_context *_pa_context;
    pa_stream *_pa_play_stream;
    pa_operation *_pa_drain;
    pa_cvolume _volume_control;

    std::atomic<bool> _initialised;

    // used to pass to operation callbacks to get the result
    struct CallbackResult
    {
        CallbackResult(pa_threaded_mainloop *mainloop) : _mainloop(mainloop), _ret(0) {}

        pa_threaded_mainloop *_mainloop;
        int _ret;
    };
};

} // namespace TTSLibrary

#endif // _PULSE_AUDIO_SINK_HPP
