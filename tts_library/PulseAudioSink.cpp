// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#include "PulseAudioSink.hpp"

#include "Log.h"
#include "TTSDefines.hpp"

#include <cstring>
#include <exception>
#include <stdexcept>

using namespace LoggingUtils;
using namespace std;

namespace TTSLibrary
{
//=============================================================================
//=============================================================================
PulseAudioSink::PulseAudioSink() : _pa_context(nullptr), _pa_play_stream(nullptr), _pa_drain(nullptr)
{
    TRACE_LOGGER;
    _initialised = false;
}

PulseAudioSink::~PulseAudioSink()
{
    TRACE_LOGGER;

    if (_initialised)
        destroy();
}

//=============================================================================
//=============================================================================
void PulseAudioSink::initialise()
{
    TRACE_LOGGER;
    LOG(Debug) << "Starting Pulse Audio Init Sequence" << endl;

    if (!_initialised)
    {
        _pa_main_loop = nullptr;

        _sample_spec.channels = 1;
        _sample_spec.rate = 16000;
        _sample_spec.format = PA_SAMPLE_S16LE;

        // bring up the pulse audio subsystem
        paMainloopInit();

        // now try and open a context
        paConnectContext();

        // connect a stream for playback
        paConnectStream();

        // full init complete
        _initialised = true;
    }
}

//=============================================================================
//=============================================================================
void PulseAudioSink::destroy()
{
    TRACE_LOGGER;
    LOG(Debug) << "Starting Pulse Audio Destroy Sequence" << endl;

    if (_initialised)
    {
        stopPlayback();
        paMainloopFinish();

        _initialised = false;
    }
}

//=============================================================================
//=============================================================================
void PulseAudioSink::stopPlayback()
{
    TRACE_LOGGER;
    LOG(Debug) << "Attempting to stop playback" << endl;
    initCheck();

    // lock the loop
    pa_threaded_mainloop_lock(_pa_main_loop);

    // little structure to get the results
    CallbackResult result(_pa_main_loop);
    auto error = 0;

    // first cork the stream, so we can flush it gracefully
    waitForOperation(pa_stream_cork(_pa_play_stream, 1, paOperationSuccess, &result));

    if (!result._ret)
    {
        auto err = CREATE_ERROR("Pulse Audio Subsystem error, pa_stream_cork: " + "failed to cork stream with error: " +
            string(pa_strerror(error)));

        LOG(Error) << err << endl;
        pa_threaded_mainloop_unlock(_pa_main_loop);
        throw runtime_error(err);
    }

    // now flush it
    waitForOperation(pa_stream_flush(_pa_play_stream, paOperationSuccess, &result));

    if (!result._ret)
    {
        auto err = CREATE_ERROR("Pulse Audio Subsystem error, pa_stream_flush: " +
            "failed to flush stream with error: " + string(pa_strerror(error)));

        LOG(Error) << err << endl;
        pa_threaded_mainloop_unlock(_pa_main_loop);
        throw runtime_error(err);
    }

    // uncork for the next playback
    waitForOperation(pa_stream_cork(_pa_play_stream, 0, paOperationSuccess, &result));

    if (!result._ret)
    {
        auto err = CREATE_ERROR("Pulse Audio Subsystem error, pa_stream_cork: " +
            "failed to uncork stream with error: " + string(pa_strerror(error)));

        LOG(Error) << err << endl;
        pa_threaded_mainloop_unlock(_pa_main_loop);
        throw runtime_error(err);
    }

    pa_threaded_mainloop_unlock(_pa_main_loop);

    LOG(Debug) << "Stopped playback" << endl;
}

//=============================================================================
//=============================================================================
void PulseAudioSink::playBuffer(SharedDataBuffer buffer)
{
    TRACE_LOGGER;
    initCheck();

    if (buffer == nullptr || buffer->size() == 0)
    {
        auto err = CREATE_ERROR("Pulse Audio Subsystem received an invalid audio buffer");
        LOG(Error) << err << endl;
        throw invalid_argument(err);
    }

    LOG(Debug) << "Playing buffer of size: " << buffer->size() << endl;

    auto error = 0;

    // we lock and free as much as possible here, mainly since
    // we want this loop to be breakable by stopPlayback()
    pa_threaded_mainloop_lock(_pa_main_loop);

    // we are going to use this callback to record bytes, and determine if the
    // success based on this
    CallbackResult play_result(_pa_main_loop);
    pa_stream_set_write_callback(_pa_play_stream, paStreamWriteCallback, &play_result);

    // only play when the stream is actually ready...
    auto stream_state = pa_stream_get_state(_pa_play_stream);

    if (stream_state == PA_STREAM_READY)
    {
        // we keep this simple, in that we dump all the data to stream then wait for
        // a drain operation to complete. This way we can pick up stopPlaying()
        // requests.
        error = pa_stream_write(
            _pa_play_stream, &(*buffer)[0], buffer->size(), paStreamWriteFreeCallback, 0, PA_SEEK_RELATIVE);

        if (error != 0)
        {
            auto err = CREATE_ERROR("Pulse Audio Subsystem error, pa_stream_write: " +
                "failed to write to stream with error: " + string(pa_strerror(error)));

            LOG(Error) << err << endl;
            pa_stream_set_write_callback(_pa_play_stream, nullptr, nullptr);
            pa_threaded_mainloop_unlock(_pa_main_loop);
            throw runtime_error(err);
        }

        CallbackResult result(_pa_main_loop);
        waitForOperation(pa_stream_drain(_pa_play_stream, paOperationSuccess, &result));

        if (!result._ret)
        {
            auto err = CREATE_ERROR("Pulse Audio Subsystem error, pa_stream_drain: " +
                "failed to drain stream with error: " + string(pa_strerror(error)));

            LOG(Error) << err << endl;
            pa_stream_set_write_callback(_pa_play_stream, nullptr, nullptr);
            pa_threaded_mainloop_unlock(_pa_main_loop);
            throw runtime_error(err);
        }
    }

    pa_stream_set_write_callback(_pa_play_stream, nullptr, nullptr);
    pa_threaded_mainloop_unlock(_pa_main_loop);
}

//=============================================================================
//=============================================================================
void PulseAudioSink::volume(int volume)
{
    TRACE_LOGGER;
    initCheck();

    // stop warnings while thisi s not implemented
    (void)volume;
}

//=============================================================================
//=============================================================================
int PulseAudioSink::volume()
{
    TRACE_LOGGER;
    initCheck();

    // not implemented yet
    return 0;
}

//=============================================================================
//=============================================================================
void PulseAudioSink::paMainloopInit()
{
    TRACE_LOGGER;
    LOG(Debug) << "Init pulse audio main loop" << endl;

    // create a threaded main loop
    if ((_pa_main_loop = pa_threaded_mainloop_new()) == nullptr)
    {
        auto err = CREATE_ERROR("Pulse Audio Subsystem error, pa_threaded_mainloop_new: " + "returned null mainloop");
        LOG(Error) << err << endl;
        throw runtime_error(err);
    }

    auto error = 0;

    // and start it
    if ((error = pa_threaded_mainloop_start(_pa_main_loop)) != 0)
    {
        auto err = CREATE_ERROR("Pulse Audio Subsystem error, pa_threaded_mainloop_start: " +
            "returned null mainloop. Error: " + string(pa_strerror(error)));

        LOG(Error) << err << endl;
        throw runtime_error(err);
    }

    LOG(Debug) << "Init complete, starting loop" << endl;
}

//=============================================================================
//=============================================================================
void PulseAudioSink::paMainloopFinish()
{
    TRACE_LOGGER;
    LOG(Debug) << "Finishing pa main loop" << endl;

    if (_pa_main_loop)
    {
        if (_pa_play_stream != nullptr)
        {
            pa_threaded_mainloop_lock(_pa_main_loop);
            pa_stream_set_state_callback(_pa_play_stream, nullptr, nullptr);
            pa_threaded_mainloop_unlock(_pa_main_loop);
        }

        if (_pa_context != nullptr)
        {
            pa_threaded_mainloop_lock(_pa_main_loop);
            pa_context_set_state_callback(_pa_context, nullptr, nullptr);
            pa_threaded_mainloop_unlock(_pa_main_loop);
        }

        pa_threaded_mainloop_stop(_pa_main_loop);
    }

    // destroy any drain operations
    if (_pa_drain != nullptr)
    {
        pa_operation_cancel(_pa_drain);
        pa_operation_unref(_pa_drain);
        _pa_drain = nullptr;
    }

    // destroy stream
    if (_pa_play_stream != nullptr)
    {
        pa_stream_disconnect(_pa_play_stream);
        pa_stream_unref(_pa_play_stream);
        _pa_play_stream = nullptr;
    }

    // destroy the context we used
    if (_pa_context != nullptr)
    {
        pa_context_disconnect(_pa_context);
        pa_context_unref(_pa_context);
        _pa_context = nullptr;
    }

    // free mainloop and exit
    if (_pa_main_loop != nullptr)
    {
        pa_threaded_mainloop_free(_pa_main_loop);
        _pa_main_loop = nullptr;
    }
}

//=============================================================================
//=============================================================================
void PulseAudioSink::paConnectContext()
{
    TRACE_LOGGER;
    LOG(Debug) << "Attempting to connect context" << endl;

    try
    {
        pa_threaded_mainloop_lock(_pa_main_loop);

        // create contect to connect to the server with
        _pa_context = pa_context_new(pa_threaded_mainloop_get_api(_pa_main_loop), "client");

        if (_pa_context == nullptr)
        {
            auto err = CREATE_ERROR(
                "Pulse Audio Subsystem error, pa_context_new: " + "failed to create a context to playback audio with");

            throw runtime_error(err);
        }

        pa_context_set_state_callback(_pa_context, paContextStateCallback, _pa_main_loop);

        // schedule conenction to server
        auto error = pa_context_connect(_pa_context, nullptr, PA_CONTEXT_NOFLAGS, nullptr);

        if (error != 0)
        {
            auto err = CREATE_ERROR("Pulse Audio Subsystem error, pa_context_connect: " +
                "failed to connect to server. Error: " + string(pa_strerror(error)));

            throw runtime_error(err);
        }

        // now wait for the context to connect
        while (pa_context_get_state(_pa_context) != PA_CONTEXT_READY)
        {
            if (!PA_CONTEXT_IS_GOOD(pa_context_get_state(_pa_context)))
            {
                auto err = CREATE_ERROR("Pulse Audio Subsystem error, " + "context failed to become ready, " +
                    "Error: " + string(pa_strerror(error)));

                throw runtime_error(err);
            }

            pa_threaded_mainloop_wait(_pa_main_loop);
        }

        pa_threaded_mainloop_unlock(_pa_main_loop);
        LOG(Debug) << "Context connected" << endl;
    }
    catch (const exception &e)
    {
        // handle the exceptions to unlock the mainloop, then rethrow
        pa_threaded_mainloop_unlock(_pa_main_loop);
        LOG(Error) << e.what() << endl;
        throw;
    }
}

//=============================================================================
//=============================================================================
void PulseAudioSink::paConnectStream()
{
    TRACE_LOGGER;
    LOG(Debug) << "Attempting to create a playback stream" << endl;

    try
    {
        pa_threaded_mainloop_lock(_pa_main_loop);

        // create the stream with the spec we initialised on init
        _pa_play_stream = pa_stream_new(_pa_context, "tts playstream", &_sample_spec, nullptr);

        if (_pa_play_stream == nullptr)
        {
            auto err = CREATE_ERROR("Pulse Audio Subsystem error, pa_stream_new: " +
                "failed to create a play stream: " + string(pa_strerror(pa_context_errno(_pa_context))));

            throw runtime_error(err);
        }

        pa_stream_set_state_callback(_pa_play_stream, paStreamStateCallback, _pa_main_loop);

        // https://freedesktop.org/software/pulseaudio/doxygen/def_8h.html#a6966d809483170bc6d2e6c16188850fc
        pa_stream_flags_t flags = PA_STREAM_VARIABLE_RATE;

        // now connect the stream, and hope for the best..
        auto error = pa_stream_connect_playback(_pa_play_stream, nullptr, nullptr, flags, nullptr, nullptr);

        if (error != 0)
        {
            auto err = CREATE_ERROR("Pulse Audio Subsystem error, pa_stream_connect_playback: " +
                "failed to connect playback stream: " + string(pa_strerror(error)));

            throw runtime_error(err);
        }

        // now wait for the stream to be ready
        while (pa_stream_get_state(_pa_play_stream) != PA_STREAM_READY)
        {
            if (!PA_STREAM_IS_GOOD(pa_stream_get_state(_pa_play_stream)))
            {
                auto err = CREATE_ERROR("Pulse Audio Subsystem error, " +
                    "stream failed to become ready, Error: " + pa_strerror(pa_context_errno(_pa_context)));

                throw runtime_error(err);
            }

            pa_threaded_mainloop_wait(_pa_main_loop);
        }

        pa_threaded_mainloop_unlock(_pa_main_loop);
        LOG(Debug) << "Playback stream created" << endl;
    }
    catch (const exception &e)
    {
        // handle the exceptions to unlock the mainloop, then rethrow
        pa_threaded_mainloop_unlock(_pa_main_loop);
        LOG(Error) << e.what() << endl;
        throw;
    }
}

//=============================================================================
//=============================================================================
pa_operation_state_t PulseAudioSink::waitForOperation(pa_operation *operation)
{
    TRACE_LOGGER;

    if (operation == nullptr)
        return PA_OPERATION_CANCELLED;

    pa_operation_state_t state = pa_operation_get_state(operation);

    // block until the operation is complete
    while (state == PA_OPERATION_RUNNING)
    {
        pa_threaded_mainloop_wait(_pa_main_loop);
        state = pa_operation_get_state(operation);
    }

    pa_operation_cancel(operation);
    pa_operation_unref(operation);

    return state;
}

//=============================================================================
//=============================================================================
void PulseAudioSink::initCheck()
{
    if (!_initialised)
    {
        auto err = CREATE_ERROR("Pulse Audio playback system is not initialised()");
        LOG(Error) << err << endl;
        throw runtime_error(err);
    }
}

//=============================================================================
//=============================================================================
void PulseAudioSink::paContextStateCallback(pa_context *context, void *user_data)
{
    TRACE_LOGGER;
    auto *mainloop = static_cast<pa_threaded_mainloop *>(user_data);
    static auto first_run = true;
    static auto old_state = pa_context_get_state(context);
    ;
    auto state = pa_context_get_state(context);

    // helpful debug information
    if ((old_state != state) || first_run)
    {
        switch (state)
        {
            case PA_CONTEXT_UNCONNECTED: LOG(Debug) << "PA: PA_CONTEXT_UNCONNECTED" << endl; break;
            case PA_CONTEXT_CONNECTING: LOG(Debug) << "PA: PA_CONTEXT_CONNECTING" << endl; break;
            case PA_CONTEXT_AUTHORIZING: LOG(Debug) << "PA: PA_CONTEXT_AUTHORIZING" << endl; break;
            case PA_CONTEXT_SETTING_NAME: LOG(Debug) << "PA: PA_CONTEXT_SETTING_NAME" << endl; break;
            case PA_CONTEXT_READY: LOG(Debug) << "PA: PA_CONTEXT_READY" << endl; break;
            case PA_CONTEXT_FAILED: LOG(Debug) << "PA: PA_CONTEXT_FAILED" << endl; break;
            case PA_CONTEXT_TERMINATED: LOG(Debug) << "PA: PA_CONTEXT_TERMINATED" << endl; break;
            default: break;
        }

        // update the old state
        old_state = state;
        first_run = false;
    }

    if (state == PA_CONTEXT_READY || !PA_CONTEXT_IS_GOOD(state))
        pa_threaded_mainloop_signal(mainloop, 0);
}

//=============================================================================
//=============================================================================
void PulseAudioSink::paStreamStateCallback(pa_stream *stream, void *user_data)
{
    TRACE_LOGGER;
    auto *mainloop = static_cast<pa_threaded_mainloop *>(user_data);
    static auto first_run = true;
    static auto old_state = pa_stream_get_state(stream);
    auto state = pa_stream_get_state(stream);

    // only process actual state changes
    if ((old_state != state) || first_run)
    {
        switch (state)
        {
            case PA_STREAM_UNCONNECTED: LOG(Debug) << "PA: PA_STREAM_UNCONNECTED" << endl; break;
            case PA_STREAM_CREATING: LOG(Debug) << "PA: PA_STREAM_CREATING" << endl; break;
            case PA_STREAM_READY: LOG(Debug) << "PA: PA_STREAM_READY" << endl; break;
            case PA_STREAM_FAILED: LOG(Debug) << "PA: PA_STREAM_READY" << endl; break;
            case PA_STREAM_TERMINATED: LOG(Debug) << "PA: PA_STREAM_READY" << endl; break;
            default: break;
        }

        // update the old state
        old_state = state;
        first_run = false;
    }

    if (state == PA_STREAM_READY || !PA_STREAM_IS_GOOD(state))
        pa_threaded_mainloop_signal(mainloop, 0);
}

//=============================================================================
//=============================================================================
void PulseAudioSink::paStreamWriteCallback(pa_stream *, size_t bytes, void *user_data)
{
    TRACE_LOGGER;
    auto *result = static_cast<CallbackResult *>(user_data);
    result->_ret += bytes;
    pa_threaded_mainloop_signal(result->_mainloop, 0);
}

//=============================================================================
//=============================================================================
void PulseAudioSink::paOperationSuccess(pa_stream *, int success, void *user_data)
{
    TRACE_LOGGER;
    auto *result = static_cast<CallbackResult *>(user_data);
    result->_ret = success;
    pa_threaded_mainloop_signal(result->_mainloop, 0);
}

//=============================================================================
//=============================================================================
void PulseAudioSink::paStreamWriteFreeCallback(void *)
{
    // do nothing
}
} // namespace TTSLibrary
