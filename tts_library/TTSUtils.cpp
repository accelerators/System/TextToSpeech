// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#include "TTSUtils.hpp"
#include "Log.h"
#include <iostream>

using namespace std;
using namespace LoggingUtils;

namespace TTSLibrary
{
//=============================================================================
//=============================================================================
pair<string, int> get_proxy_settings()
{
    string proxy_env = "";
    string proxy = "";
    int port = 0;

    if (getenv("HTTP_PROXY") != nullptr)
    {
        proxy_env = getenv("HTTP_PROXY");
        LOG(Debug) << "Read proxy setting: " << proxy_env << " from environment" << endl;
    }

    if (!proxy_env.empty())
    {
        string port_str = proxy_env.substr(proxy_env.find_last_of(":") + 1, proxy_env.size());
        proxy = proxy_env.substr(0, proxy_env.find_last_of(":"));

        if (!port_str.empty())
        {
            try
            {
                port = stoi(port_str);
            }
            catch (const exception &e)
            {
                LOG(Error) << "Unable to convert port number for proxy setting" << endl;
                proxy = "";
                port = 0;
            }
        }
        else
        {
            LOG(Debug) << "No proxy port setting detected" << endl;
            proxy = "";
        }
    }
    else
    {
        LOG(Debug) << "No proxy setting detected" << endl;
        proxy = "";
    }

    return pair<string, int>(proxy, port);
}

} // namespace TTSLibrary
