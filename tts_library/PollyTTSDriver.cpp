// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#include "PollyTTSDriver.hpp"

#include "Log.h"
#include "TTSDefines.hpp"

#include <aws/core/utils/Outcome.h>
#include <aws/polly/PollyClient.h>
#include <aws/polly/model/DescribeVoicesRequest.h>
#include <aws/polly/model/Gender.h>
#include <aws/polly/model/LanguageCode.h>
#include <aws/polly/model/SynthesizeSpeechRequest.h>

using namespace LoggingUtils;
using namespace std;
using namespace Aws;
using namespace Aws::Polly;
using namespace Aws::Polly::Model;
using namespace Aws::Client;
using namespace Aws::Utils;

namespace TTSLibrary
{
//=============================================================================
//=============================================================================
PollyTTSDriver::PollyTTSDriver()
{
    TRACE_LOGGER;
}

//=============================================================================
//=============================================================================
PollyTTSDriver::~PollyTTSDriver()
{
    TRACE_LOGGER;
    Aws::ShutdownAPI(_tts_options);
}

//=============================================================================
//=============================================================================
void PollyTTSDriver::initialise(const std::string &proxy, int proxy_port)
{
    TRACE_LOGGER;
    LOG(Debug) << "Init Driver" << endl;

    // Bring up the AWS SDK
    // todo optionise this, and use our own logger
    _tts_options.loggingOptions.logLevel = AwsLoggingLevel;
    Aws::InitAPI(_tts_options);

    ClientConfiguration config;

    if (!proxy.empty())
    {
        LOG(Debug) << "Proxy passed as: " << proxy << ":" << proxy_port << endl;
        config.proxyHost = proxy.c_str();
        config.proxyPort = proxy_port;
    }

    _polly_client = Aws::MakeShared<PollyClient>(AppName.c_str(), config);

    collectVoiceInfo();

    if (_pretty_voices.empty())
    {
        auto err = CREATE_ERROR(
            "No valid voices found to use in text to speech. " + "Perhaps the network connection failed.");

        LOG(Error) << err << endl;
        throw runtime_error(err);
    }
}

//=============================================================================
//=============================================================================
void PollyTTSDriver::destroy()
{
    TRACE_LOGGER;
}

//=============================================================================
//=============================================================================
SharedDataBuffer PollyTTSDriver::getSpeechBuffer(const string &text, const string &pretty_voice)
{
    TRACE_LOGGER;
    LOG(Debug) << "Requesting text: " << text << endl;

    SharedDataBuffer buffer;
    SynthesizeSpeechRequest speech_request;

    // get the voice id for this voice
    VoiceId voice_id = prettyVoiceToVoiceId(pretty_voice);

    if (voice_id == VoiceId::NOT_SET)
    {
        auto err = CREATE_ERROR("Invalid voice selection for this speech request: " + pretty_voice);
        LOG(Error) << err << endl;
        throw runtime_error(err);
    }

    // prepare a request based on the text and class variables
    speech_request.WithOutputFormat(_sample_format)
        .WithSampleRate(_sample_rate.c_str())
        .WithTextType(TextType::text)
        .WithText(text.c_str())
        .WithVoiceId(voice_id);

    // send the actual request
    SynthesizeSpeechOutcome outcome = _polly_client->SynthesizeSpeech(speech_request);

    if (outcome.IsSuccess())
    {
        // the result comes as a stream
        auto result = const_cast<Polly::Model::SynthesizeSpeechOutcome &>(outcome).GetResultWithOwnership();
        auto &stream = result.GetAudioStream();

        // magic time, get the data from the stream copied into the buffer
        buffer = make_shared<const DataBuffer>((istreambuf_iterator<char>(stream)), istreambuf_iterator<char>());
    }
    else
    {
        // to deal with the error we throw an exception, this will be
        // picked up on one of the worker threads
        auto error = outcome.GetError();

        auto err = CREATE_ERROR("An error occurred when contacting Aws services. SynthesizeSpeech request error: " +
            string(error.GetExceptionName().c_str()) + ", " + string(error.GetMessage().c_str()));

        LOG(Error) << err << endl;
        throw runtime_error(err);
    }

    return buffer;
}

//=============================================================================
//=============================================================================
void PollyTTSDriver::collectVoiceInfo()
{
    TRACE_LOGGER;

    // Attempt to get information about each language requested in the defines
    for (auto &code : LanguageCodes)
    {
        // we request only British English speaking voices
        DescribeVoicesRequest describe_request;
        describe_request.WithLanguageCode(code);

        auto request_result = _polly_client->DescribeVoices(describe_request);

        if (request_result.IsSuccess())
        {
            // gather some additional information aout the voice for the user,
            // we simply chop this off when its passed back to us
            for (auto &voice : request_result.GetResult().GetVoices())
            {
                _pretty_voices.push_back(string(voice.GetName().c_str()) + " (" +
                    string(GenderMapper::GetNameForGender(voice.GetGender()).c_str()) + ") " +
                    string(LanguageCodeMapper::GetNameForLanguageCode(voice.GetLanguageCode()).c_str()));
            }
        }
        else
        {
            // to deal with the error we throw an exception, this will be
            // picked up on one of the worker threads
            auto error = request_result.GetError();

            auto err = CREATE_ERROR("An error occurred when contacting Aws services. DescribeVoices request error: " +
                string(error.GetExceptionName().c_str()) + ", " + string(error.GetMessage().c_str()));

            LOG(Error) << err << endl;
            throw runtime_error(err);
        }
    }

    LOG(Debug) << "Found: " << _pretty_voices.size() << " available voices" << endl;
}

//=============================================================================
//=============================================================================
Aws::Polly::Model::VoiceId PollyTTSDriver::prettyVoiceToVoiceId(const std::string pretty_voice)
{
    auto voice = pretty_voice.substr(0, pretty_voice.find_first_of(" "));
    return VoiceIdMapper::GetVoiceIdForName(voice.c_str());
}

} // namespace TTSLibrary
