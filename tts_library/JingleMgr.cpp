// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#include "JingleMgr.hpp"
#include "Log.h"
#include "TTSDefines.hpp"

#include <algorithm>
#include <exception>
#include <experimental/filesystem>
#include <fstream>
#include <iterator>
#include <sstream>

namespace fs = std::experimental::filesystem;
using namespace std;
using namespace LoggingUtils;

namespace TTSLibrary
{
//=============================================================================
//=============================================================================
JingleMgr::JingleMgr(const string &search_directory)
{
    TRACE_LOGGER;
    updateSearchDirectory(search_directory);
}

//=============================================================================
//=============================================================================
vector<string> JingleMgr::availableJingles() const
{
    TRACE_LOGGER;

    vector<string> entries;

    // load files ending in pcm as possible jingles
    for (auto &item : fs::directory_iterator(_search_directory))
    {
        if (fs::is_regular_file(item))
        {
            // for readability
            string full_path_name = item.path().string();

            // only look at files ending in pcm
            if (full_path_name.substr(full_path_name.find_last_of(".") + 1) == "pcm")
            {
                // trim the path from the name
                string trimmed_name = full_path_name;

                if (full_path_name.find_last_of("/") != string::npos)
                    trimmed_name = full_path_name.substr(full_path_name.find_last_of("/") + 1);

                // record it
                entries.push_back(trimmed_name);
            }
        }
    }

    LOG(Debug) << "Found " << entries.size() << " available jingles in " << _search_directory << endl;
    return entries;
}

//=============================================================================
//=============================================================================
void JingleMgr::updateSearchDirectory(const string &search_directory)
{
    TRACE_LOGGER;
    LOG(Debug) << "Setting search directory as: " << search_directory << endl;

    if (search_directory.empty())
    {
        auto err = CREATE_ERROR("Cannot use a blank string as the jingle location");
        LOG(Error) << err << endl;
        throw invalid_argument(err);
    }

    if (search_directory == ".")
    {
        auto err = CREATE_ERROR("Cannot use the current directory string as the jingle location");
        LOG(Error) << err << endl;
        throw invalid_argument(err);
    }

    // check we have a viable directory to attempt to load from
    if (fs::exists(search_directory))
    {
        if (!fs::is_directory(search_directory))
        {
            auto err = CREATE_ERROR(
                "Requested location: " + search_directory + " is invalid or not a directory. Choose a new location.");

            LOG(Error) << err << endl;
            throw invalid_argument(err);
        }
    }
    else
    {
        auto err = CREATE_ERROR("Requested location: " + search_directory + " does not exist. Choose a new location.");

        LOG(Error) << err << endl;
        throw invalid_argument(err);
    }

    _search_directory = search_directory;
    _cached_jingle = nullptr;
    _last_loaded_jingle = "";
}

//=============================================================================
//=============================================================================
bool JingleMgr::jingleExists(const string &name) const
{
    TRACE_LOGGER;
    vector<string> entries = availableJingles();
    return find(entries.begin(), entries.end(), name) != entries.end();
}

//=============================================================================
//=============================================================================
string JingleMgr::jingleCached() const
{
    TRACE_LOGGER;
    return _last_loaded_jingle;
}

//=============================================================================
//=============================================================================
SharedDataBuffer JingleMgr::getAudioBuffer(const string &name)
{
    TRACE_LOGGER;
    LOG(Debug) << "Requesting jingle: " << name << endl;

    if (name.empty())
    {
        string err = CREATE_ERROR("No jingle name given");
        LOG(Error) << err << endl;
        throw runtime_error(err);
    }

    // see if its cached
    if (name == _last_loaded_jingle)
    {
        LOG(Debug) << "FROM CACHE: " << name << endl;
        // yes, return the cache
        return _cached_jingle;
    }

    vector<string> entries = availableJingles();

    // this is a bad request, return
    if (find(entries.begin(), entries.end(), name) == entries.end())
    {
        string err = CREATE_ERROR(
            "Request for unavailable jingle. Has the file been deleted or moved? Jingle: " + name);

        LOG(Error) << err << endl;
        throw runtime_error(err);
    }

    // now load the jingle and cache it, to short cut all this next time
    ifstream file;
    file.exceptions(ios::failbit);

    try
    {
        // will throw iso_base::failure exception on failure
        file.open(_search_directory + "/" + name, ios::in | ios::binary);
    }
    catch (const exception &e)
    {
        string err = CREATE_ERROR(
            "An unknown error occurred when trying to load jingle: " + name + ", error: " + e.what());

        LOG(Error) << err << endl;
        file.close();
        throw runtime_error(err);
    }

    file.seekg(0, ios::end);
    streampos size = file.tellg();
    file.seekg(0, ios::beg);

    // make sure the file has some data in it
    if (size <= 0)
    {
        string err = CREATE_ERROR("Requested jingle is an invalid file: " + name);
        LOG(Error) << err << endl;
        file.close();
        throw runtime_error(err);
    }

    // some magic, reading directly from file into the buffer inside the shared ptr
    _cached_jingle = make_shared<const DataBuffer>((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
    file.close();
    LOG(Debug) << "Loaded and cached audio jingle: " << name << endl;

    // update the cached jingle string
    _last_loaded_jingle = name;

    // now its loaded and cached, we can return it to the caller
    return _cached_jingle;
}

} // namespace TTSLibrary
