// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _POLLY_TTSDRIVER_HPP
#define _POLLY_TTSDRIVER_HPP

#include "TTSDataTypes.hpp"
#include "TTSDefines.hpp"

#include <aws/core/Aws.h>
#include <aws/polly/PollyClient.h>
#include <aws/polly/model/OutputFormat.h>
#include <string>
#include <vector>

namespace TTSLibrary
{
// This class handles the text to speech requests to Aws services. This is basically
// a wrapper around all the amazon code with a blocking call to make the text to
// speech conversion. We convert all amazon types into basic C++ types, to stop
// polluting our namespaces and files with amazon defines/typedef
class PollyTTSDriver
{
public:
    PollyTTSDriver();
    ~PollyTTSDriver();

    // This call will connect to amazon services
    void initialise(const std::string &proxy = "", int proxy_port = 0);

    void destroy();

    // Voice selection API
    std::vector<std::string> availableVoices() const { return _pretty_voices; }

    // Attempt to turn the requested speech into a buffer, via
    // either the cache or Amazon online services. This is completed
    // as a blocking call
    SharedDataBuffer getSpeechBuffer(const std::string &text, const std::string &pretty_voice);

private:
    // build the _pretty_voices vector
    void collectVoiceInfo();

    // convert to a voice we can use to select one in the aws model
    Aws::Polly::Model::VoiceId prettyVoiceToVoiceId(const std::string pretty_voice);

    // AWS Polly variables
    Aws::SDKOptions _tts_options;
    std::shared_ptr<Aws::Polly::PollyClient> _polly_client;
    std::atomic<Aws::Polly::Model::VoiceId> _current_voice_id;

    // sample data, this could be made a class parameter at a later date
    Aws::Polly::Model::OutputFormat _sample_format = Aws::Polly::Model::OutputFormat::pcm;
    std::string _sample_rate = "16000";

    // voice cache, pretty because it contains gender and country info
    std::vector<std::string> _pretty_voices;
};

} // namespace TTSLibrary

#endif // _POLLY_TTSDRIVER_HPP
