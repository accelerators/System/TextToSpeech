// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#include "TTSRunner.hpp"

#include "FileTTSCache.hpp"
#include "JingleMgr.hpp"
#include "Log.h"
#include "PollyTTSDriver.hpp"
#include "PulseAudioSink.hpp"
#include "TTSDefines.hpp"

#include <future>

using namespace std;
using namespace LoggingUtils;

namespace TTSLibrary
{
//=============================================================================
//=============================================================================
TTSRunner::TTSRunner() : _init_complete(false), _speech_delay(MsSpeechDelayDefault)
{
    TRACE_LOGGER;
}

//=============================================================================
//=============================================================================
TTSRunner::~TTSRunner()
{
    TRACE_LOGGER;

    if (_init_complete)
        destroy();
}

//=============================================================================
//=============================================================================
void TTSRunner::initialise(const std::string &proxy, int proxy_port)
{
    TRACE_LOGGER;
    LOG(Info) << "Initialising the TTS Runner and its worker thread" << endl;

    // allocate all the various sub systems and bring them up
    _polly_driver = new PollyTTSDriver();
    _audio_sink = new PulseAudioSink();

    // polly and pulse can be slow to bring up, so spin them out
    // onto async threads and wait for them.
    auto polly_init = async(std::launch::async, [&_polly_driver = _polly_driver, proxy, proxy_port]() { 
        _polly_driver->initialise(proxy, proxy_port); 
        });

    auto audio_init = async(std::launch::async, [&_audio_sink = _audio_sink]() { 
        _audio_sink->initialise(); 
        });

    polly_init.wait();
    audio_init.wait();

    // make sure this are nullptrs
    _audio_cache = nullptr;
    _jingle_mgr = nullptr;

    _cache_hits = 0;
    _polly_requests = 0;

    // everything should be valid, start are main thread
    _exit_thread = false;
    _worker_thread = thread(&TTSRunner::mainLoop, this);
    _init_complete = true;
}

//=============================================================================
//=============================================================================
void TTSRunner::initialiseCache(const std::string &cache_location, const string &fallback_message)
{
    TRACE_LOGGER;
    LOG(Info) << "Initialising the caching system" << endl;
    checkInitState();
    lock_guard<mutex> lock(_access_mutex);
    _audio_cache = new FileTTSCache(cache_location, true);

    if (!fallback_message.empty())
    {
        // when using the cache we can cache an emergency message to use when
        // the polly connection fails. Here we ensure this message is cached in
        // each voice
        vector<string> voices = _polly_driver->availableVoices();
        vector<future<bool>> results;

        for (auto voice : voices)
        {
            results.push_back(async(std::launch::async, [this, &fallback_message, voice]() {
                try
                {
                    // only request the speech buffers for those
                    // missing in the cache
                    if (!_audio_cache->isCached(fallback_message, voice))
                    {
                        auto buffer = _polly_driver->getSpeechBuffer(fallback_message, voice);
                        _audio_cache->cache(fallback_message, buffer, voice);
                        _polly_requests++;
                    }
                }
                catch (const exception &e)
                {
                    // something went wrong, communicate it back, we
                    // can live without the cached messages, but we  want to
                    // know about the error
                    return false;
                }

                return true;
            }));
        }

        // there is no wait for all in c++ yet, so wait on each individually
        for (auto result = results.begin(); result != results.end(); ++result)
        {
            if ((*result).get() == false)
                LOG(Error) << "Unable to cache 1 or more connectivity loss messages" << endl;
        }
    }

    // update, even if blank, if blank then it means no message
    _fallback_message = fallback_message;
}

//=============================================================================
//=============================================================================
void TTSRunner::initialiseJingle(const std::string &jingle_location)
{
    TRACE_LOGGER;
    LOG(Info) << "Initialising the jingle system" << endl;
    checkInitState();
    lock_guard<mutex> lock(_access_mutex);
    _jingle_mgr = new JingleMgr(jingle_location);
}

//=============================================================================
//=============================================================================
void TTSRunner::destroy()
{
    TRACE_LOGGER;
    LOG(Info) << "Destroying the TTS Runner and joining its worker thread" << endl;

    if (_worker_thread.joinable())
    {
        _exit_thread = true;
        _worker_thread.join();
        LOG(Debug) << "Joined  worker thread" << endl;
    }

    if (_jingle_mgr != nullptr)
    {
        delete _jingle_mgr;
        _jingle_mgr = nullptr;
    }

    if (_audio_cache != nullptr)
    {
        delete _audio_cache;
        _audio_cache = nullptr;
    }

    if (_polly_driver != nullptr)
    {
        _polly_driver->destroy();
        delete _polly_driver;
        _polly_driver = nullptr;
    }

    if (_audio_sink != nullptr)
    {
        _audio_sink->destroy();
        delete _audio_sink;
        _audio_sink = nullptr;
    }

    _init_complete = false;
}

//=============================================================================
//=============================================================================
future<bool> TTSRunner::addRequest(const TTSRequest &request)
{
    TRACE_LOGGER;
    LOG(Info) << "Pushing request to speak text: " << request._text << ", voice: " << request._voice << endl;
    checkInitState();

    // wrap the request inside an internal structure for conviences
    TTSReqInternal req_internal;
    req_internal._data = request;

    // create a future for the result to be passed back to the caller
    future<bool> result = req_internal._result.get_future();

    // now push the request to be picked up by the worker thread later
    _request_queue.push(std::move(req_internal));

    return move(result);
}

//=============================================================================
//=============================================================================
void TTSRunner::stopPlayback()
{
    TRACE_LOGGER;
    LOG(Info) << "Requesting any current audio message playback is stopped" << endl;
    checkInitState();

    // we do not need to lock these calls, since pulse audio has its own locking
    // implemented in the sink. Further, using _access_mutex would conflict
    // with the main loop taking the lock
    _audio_sink->stopPlayback();

    // crude, but we can not do this any other way, signal the worker
    // to quit tts early
    _cancel_current_tts = true;
}

//=============================================================================
//=============================================================================
void TTSRunner::clearAllRequests()
{
    TRACE_LOGGER;
    LOG(Info) << "Clearing any queued message requests" << endl;
    checkInitState();
    _request_queue.clear();
}

//=============================================================================
//=============================================================================
void TTSRunner::setVolume(int volume)
{
    TRACE_LOGGER;
    LOG(Debug) << "Adjusting volume to: " << volume << endl;
    checkInitState();

    // no need to lock, see stopPlayback()
    _audio_sink->volume(volume);
}

//=============================================================================
//=============================================================================
vector<string> TTSRunner::availableVoices() const
{
    TRACE_LOGGER;
    LOG(Debug) << "Loading available voices" << endl;
    checkInitState();
    lock_guard<mutex> lock(_access_mutex);

    // get the voices and sort alphabetically
    vector<string> voices = _polly_driver->availableVoices();
    sort(voices.begin(), voices.end());

    return voices;
}

//=============================================================================
//=============================================================================
bool TTSRunner::validateVoice(const string &voice_name) const
{
    TRACE_LOGGER;
    LOG(Debug) << "Checking voice valid: " << voice_name << endl;
    checkInitState();
    lock_guard<mutex> lock(_access_mutex);

    vector<string> voices = _polly_driver->availableVoices();

    // make sure the user is not requesting rubbish
    if (find(voices.begin(), voices.end(), voice_name) == voices.end())
    {
        LOG(Debug) << "Voice " << voice_name << " is not valid" << endl;
        return false;
    }

    LOG(Debug) << "Voice " << voice_name << " valid" << endl;
    return true;
}

//=============================================================================
//=============================================================================
vector<string> TTSRunner::availableJingles() const
{
    TRACE_LOGGER;
    LOG(Debug) << "Loading available jingles" << endl;
    checkInitState();
    lock_guard<mutex> lock(_access_mutex);

    if (_jingle_mgr == nullptr)
    {
        auto err = CREATE_ERROR("Jingle subsystem not initialised to usage. First call initialiseJingle()");
        LOG(Error) << err << endl;
        throw runtime_error(err);
    }

    // get the voices and sort alphabetically
    vector<string> jingles = _jingle_mgr->availableJingles();
    sort(jingles.begin(), jingles.end());

    return jingles;
}

//=============================================================================
//=============================================================================
bool TTSRunner::validateJingle(const string &jingle_name) const
{
    TRACE_LOGGER;
    LOG(Debug) << "Checking jingle valid: " << jingle_name << endl;
    lock_guard<mutex> lock(_access_mutex);
    checkInitState();

    if (_jingle_mgr == nullptr)
    {
        auto err = CREATE_ERROR("Jingle subsystem not initialised to usage. First call initialiseJingle()");
        LOG(Error) << err << endl;
        throw runtime_error(err);
    }

    // ensure it exists
    if (!_jingle_mgr->jingleExists(jingle_name))
    {
        LOG(Debug) << "Jingle " << jingle_name << " is not valid" << endl;
        return false;
    }

    LOG(Debug) << "Jingle " << jingle_name << " valid" << endl;
    return true;
}


//=============================================================================
//=============================================================================
int TTSRunner::speechDelay() const
{
    // just return this, could be called a lot by the device server,
    // so no debug
    return _speech_delay;
}

//=============================================================================
//=============================================================================
bool TTSRunner::speechDelay(int ms)
{
    TRACE_LOGGER;
    LOG(Debug) << "Updating speech delay: " << ms << endl;

    // ignore silly requests
    if (ms < 0 || ms > 1000)
        return false;

    _speech_delay = ms;
    return true;
}

//=============================================================================
//=============================================================================
void TTSRunner::checkInitState() const
{
    if (_init_complete == false)
    {
        auto err = CREATE_ERROR(
            "Unable to use the TTS API until initialisation is complete. " + "Ensure initialise() has been called.");

        LOG(Error) << err << endl;
        throw runtime_error(err);
    }
}

//=============================================================================
//=============================================================================
void TTSRunner::mainLoop()
{
    TRACE_LOGGER;
    LOG(Info) << "Starting mainloop worker thread" << endl;

    try
    {
        while (!_exit_thread)
        {
            // wait for the next request to be pushed onto the queue, this
            // blocks until a string is pushed onto the queue
            TTSReqInternal req;
            _request_queue.pop(req, RequestQueuePopTimeout);

            // either we got a string or woke up after the timeout, if after the timeout
            // then we just skip the loop and check the exit flag
            if (req._data._text.empty())
            {
                req._result.set_value(false);
                continue;
            }

            try
            {
                // this flag allows us to cancel this playback midway through. We need this
                // since the jingle and speech are two seperate playbacks, and a stopPlayback
                // needs to be able to cancel the jingle and then not play the speech. The
                // flag is set to true in stopPlayback, which will ask pulse audio to
                // cancel playback.
                _cancel_current_tts = false;

                // take the access mutex for the duration of the jingle, this seems a little
                // much, but we do not want cache locations changing as we use various objects.
                // this lock will auto unlock on destruction, i.e. if we have an exception,
                // otherwise
                // we unlike it asap
                unique_lock<mutex> lock(_access_mutex);

                // first job is to request the speech from Polly, this can take up
                // to 700ms, so its done in async to us playing the jingle. Its possible
                // the speech buffer was cached and we will have the audio ready
                // quicker than an amazon lookup.
                auto polly_result = async(std::launch::async,
                    [this](const string &text, const string &voice) -> SharedDataBuffer {
                        SharedDataBuffer buffer;

                        if (cacheEnabled())
                        {
                            // first check the cache, if available then we use the cached buffer.
                            // Notice we wrap this in a try/catch, if there is a problem with
                            // file access, then we can recover the situation by just using the
                            // polly text to speech engine
                            try
                            {
                                if (_audio_cache->isCached(text, voice))
                                    buffer = _audio_cache->retrieveCacheEntry(text, voice);
                            }
                            catch (const exception &e)
                            {
                                // record any errors
                                LOG(Error) << "Caught exception in async polly request: " << e.what() << endl;
                                ExceptionQueue.push(current_exception());
                            }

                            // if we did not get a valid buffer, use polly
                            if (buffer == nullptr)
                            {
                                buffer = _polly_driver->getSpeechBuffer(text, voice);
                                _audio_cache->cache(text, buffer, voice);
                                _polly_requests++;

                                LOG(Info) << "Looked up <" << text << "> speech buffer from polly due to cache failure"
                                          << endl;
                            }
                            else
                            {
                                // valid cache hit, increment the stats
                                _cache_hits++;
                                LOG(Info) << "Using cached <" << text << "> speech buffer" << endl;
                            }
                        }
                        else
                        {
                            // no caching, always try to get it from amazon
                            buffer = _polly_driver->getSpeechBuffer(text, voice);
                            _polly_requests++;
                            LOG(Info) << "Looked up <" << text << "> speech buffer from polly" << endl;
                        }

                        return buffer;
                    },
                    req._data._text, req._data._voice);

                // while we are fetching the speech audio, we play the jingle (if requested),
                // this covers the fact that it can take 700ms to get the speech from AWS
                future<bool> jingle_result;

                // play a jingle if one was requested
                if (jingleEnabled() && req._data.jingleRequested())
                {
                    jingle_result = async(std::launch::async,
                        [this](const string &jingle_name) -> bool {
                            _audio_sink->playBuffer(_jingle_mgr->getAudioBuffer(jingle_name));
                            return !_cancel_current_tts;
                        },
                        req._data._jingle);
                }

                // get the speech buffer from the polly async call, this will wait until it
                // completes. While this is on going, the jingle is playing (if requested)
                SharedDataBuffer speech_buffer = polly_result.get();

                // now, if the jingle was requested and is valid, we need to ensure we wait
                // for it to finish, this will block until the jingle is finished
                if (speech_buffer == nullptr ||
                    (jingleEnabled() && req._data.jingleRequested() && jingle_result.get() == false) || _exit_thread)
                {
                    // something went wrong, or the playback was aborted, skip
                    // the rest of this tts
                    lock.unlock();
                    LOG(Info) << "Speech request broken (possible exit request)" << endl;
                    req._result.set_value(false);
                    continue;
                }

                // unlock the lock before we start speech playback, this will allow the owning
                // object to make basic changes to config before the next iteration. This is safe
                // as the pulse audio sink class has its own locking based on pulse audio
                lock.unlock();

                // before we play the speech,, we put in a short pause to give a moments break,
                // and check for a break request
                if (jingleEnabled() && _speech_delay > 0)
                    this_thread::sleep_for(chrono::milliseconds(_speech_delay));

                // check no exit request before blocking on speech
                if (_exit_thread)
                {
                    LOG(Info) << "Speech request broken (exit request)" << endl;
                    req._result.set_value(false);
                    continue;
                }

                // now grab the result of the polly request and attempt to play it,
                _audio_sink->playBuffer(speech_buffer);

                // if we reached this far, chances are the audio was played, so
                // trigger the future with an indication of success
                LOG(Info) << "Speech request successful" << endl;
                req._result.set_value(true);
            }
            catch (const exception &e)
            {
                req._result.set_value(false);

                LOG(Error) << "Caught exception on worker thread: " << e.what() << endl;

                // catch all exceptions from the ps worker thread, and from
                // here post them to the exception queue. THe main thread can
                // then pick them up and report them up to the user
                ExceptionQueue.push(current_exception());
            }
        }
    }
    catch (const exception &e)
    {
        // set the exit state, this way if running() is queried
        // then the sink reflects the stopped state
        _exit_thread = true;

        // catch all exceptions from the ps worker thread, and from
        // here post them to the exception queue. THe main thread can
        // then pick them up and report them up to the user
        ExceptionQueue.push(current_exception());
    }

    LOG(Info) << "Exciting mainloop worker thread" << endl;
}
} // namespace TTSLibrary
