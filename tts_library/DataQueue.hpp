// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _DATA_QUEUE_HPP
#define _DATA_QUEUE_HPP

#include <chrono>
#include <condition_variable>
#include <mutex>
#include <queue>

namespace TTSLibrary
{
// Simple locked queue for handling some events and data paths
// in the tts library. Note we implement move push/pop methods
// to allow us to push/pop complex objects.
template<class T>
class DataQueue
{
public:
    DataQueue() : _queue(), _mutex(), _cond() {}
    ~DataQueue() {}

    // Push an item onto the queue, locking it while the operation completes.
    void push(const T &t)
    {
        std::unique_lock<std::mutex> lock(_mutex);

        // push the item, notify and exit, which will free the lock
        _queue.push(t);
        _cond.notify_one();
    }

    // Push an item onto the queue, locking it while the operation completes.
    // This version takes the result of a move constructor.
    void push(T &&t)
    {
        std::unique_lock<std::mutex> lock(_mutex);

        // push the item, notify and exit, which will free the lock
        _queue.push(std::move(t));
        _cond.notify_one();
    }

    // Pop an item from the queue, by value, and locking it while the
    // operation completes. In this case, if the queue is empty, we
    // wait until an item is available
    T pop()
    {
        std::unique_lock<std::mutex> lock(_mutex);

        while (_queue.empty())
        {
            // release lock as long as the wait and reacquire it afterwards.
            _cond.wait(lock);
        }

        auto t = _queue.front();
        _queue.pop();
        return t;
    }

    // Pop an item from the queue, locking it while the operation completes.
    // In this case, if the queue is empty, we wait until an item is available,
    bool pop(T &t)
    {
        std::unique_lock<std::mutex> lock(_mutex);

        while (_queue.empty())
            _cond.wait(lock);

        t = std::move(_queue.front());
        _queue.pop();
        return true;
    }

    // Like pop(T), but here we can specify a time out
    bool pop(T &t, int ms)
    {
        auto const timeout = std::chrono::steady_clock::now() + std::chrono::milliseconds(ms);
        std::unique_lock<std::mutex> lock(_mutex);

        while (_queue.empty())
            if (_cond.wait_until(lock, timeout) == std::cv_status::timeout)
                break;

        if (_queue.empty())
            return false;

        t = std::move(_queue.front());
        _queue.pop();
        return true;
    }

    // Get the empty status of the queue
    bool empty()
    {
        std::lock_guard<std::mutex> lock(_mutex);
        return _queue.empty();
    }

    // Get the queue size
    int size()
    {
        std::lock_guard<std::mutex> lock(_mutex);
        return _queue.size();
    }

    // Clear all remaining items on the queue
    void clear()
    {
        std::lock_guard<std::mutex> lock(_mutex);

        while (!_queue.empty())
            _queue.pop();
    }

private:
    std::queue<T> _queue;
    mutable std::mutex _mutex;
    std::condition_variable _cond;
};

} // namespace TTSLibrary

#endif // _DATA_QUEUE_HPP
