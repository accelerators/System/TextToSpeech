// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TTS_DATA_TYPES_HPP
#define _TTS_DATA_TYPES_HPP

#include "DataQueue.hpp"

#include <exception>
#include <memory>
#include <vector>

namespace TTSLibrary
{
// typedef the basic types used for passing around audio buffers,
// this helps readability and maintainability
typedef unsigned char DataByte;
typedef std::vector<DataByte> DataBuffer;
typedef std::shared_ptr<const DataBuffer> SharedDataBuffer;

// this queue handles exceptions from the worker threads,
// and allows them to be passed back to the main thread
extern DataQueue<std::exception_ptr> ExceptionQueue;

} // namespace TTSLibrary

#endif // _TTS_DATA_TYPES_HPP
