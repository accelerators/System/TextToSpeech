// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _JINGLE_MGR_HPP
#define _JINGLE_MGR_HPP

#include "TTSDataTypes.hpp"
#include <memory>
#include <string>
#include <vector>

namespace TTSLibrary
{
// Wraps up management of the jingles to be played before text to speech
// audio playback. Caches the last request to provide a good value memory
// to cpu cycles speed up. No internal caching of whats on disk, this is
// to much given how rarely the selected jingle changes (we can afford
// to read the disk again).
class JingleMgr
{
public:
    // the search directory must be a valid directory, that is neither blank
    // nor the current run directory, i.e. we want a separate dir full
    // of pcm files
    JingleMgr(const std::string &search_directory);
    ~JingleMgr() {}

    // Just return a list of everything we think is a valid jingle
    // in the directory
    std::vector<std::string> availableJingles() const;

    // Check the requested jingle exists, useful for verifying user
    // selection of a jingle
    bool jingleExists(const std::string &name) const;

    // check if a jingle is cached from last loading
    std::string jingleCached() const;

    // Load and return as a buffer the selected jingle
    SharedDataBuffer getAudioBuffer(const std::string &name);

    // change the search directory for jingles
    void updateSearchDirectory(const std::string &search_directory);

private:
    // current search directory
    std::string _search_directory;

    // quick cache, save us loading the jingle over and over...
    std::string _last_loaded_jingle;
    SharedDataBuffer _cached_jingle;
};

} // namespace TTSLibrary

#endif // _JINGLE_MGR_HPP
