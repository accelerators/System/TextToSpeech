// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TTS_RUNNER_HPP
#define _TTS_RUNNER_HPP

#include "DataQueue.hpp"

#include <atomic>
#include <future>
#include <memory>
#include <mutex>
#include <string>
#include <thread>

namespace TTSLibrary
{
// Forward declares
class PollyTTSDriver;
class PulseAudioSink;
class JingleMgr;
class FileTTSCache;

// This is a manager object for all the various sub components. It both controls
// access to them, and how they are used. Internally the class has a worker thread
// that all speech requests are handed off to, this then attempt to complete the
// request without blocking.
class TTSRunner
{
public:

    // This simple structure forms the basis of a speech request.
    // Each request much be at least some text and a voice, but
    // if a jingle is requested then this is played first.
    struct TTSRequest
    {
        bool jingleRequested() const { return !_jingle.empty(); }
        std::string _text;
        std::string _voice;
        std::string _jingle;
    };

    TTSRunner();
    ~TTSRunner();

    // Configuration and Setup --------------------

    // the stepped init process means we can bring up the base system, and only
    // init other features we are interested in. By doing this, we can catch
    // errors from them systems and handle them differently
    void initialise(const std::string &proxy = "", int proxy_port = 0);

    // if fallback_message is empty, no fallback audio will be generated
    void initialiseCache(const std::string &cache_location, const std::string &fallback_message);
    void initialiseJingle(const std::string &jingle_location);
    bool initialised() const { return _init_complete; }
    void destroy();

    // Voice selection functions
    std::vector<std::string> availableVoices() const;
    bool validateVoice(const std::string &voice_name) const;

    // Jingle selection functions
    std::vector<std::string> availableJingles() const;
    bool validateJingle(const std::string &jingle_name) const;

    // configure the delay between the jingle and speech. A valid
    // value is between 0 and 1000, with 0 switching the delay off
    int speechDelay() const;
    bool speechDelay(int ms);

    // Runtime  --------------------

    // Add a request to the runner to play an alert (jingle + speech),
    // this will be processed when the worker thread has finished any
    // items ahead of it.
    std::future<bool> addRequest(const TTSRequest &request);

    // Clear the request queue (does not stop current playback, this
    // must be called separately)
    void clearAllRequests();

    // Attempt to cancel the current item being played
    void stopPlayback();

    // Request the runner change the volume
    void setVolume(int volume);

    // Stats and Debug --------------------
    unsigned long cacheHits() const { return _cache_hits; }
    unsigned long pollyRequests() const { return _polly_requests; }

    void clearStats()
    {
        _cache_hits = 0;
        _polly_requests = 0;
    }

private:

    // this is the internal version of the TTS request. We use this since
    // we want to store some stats and a promise to send the result back
    // to the caller. It wraps the original request.
    struct TTSReqInternal
    {
        TTSReqInternal() {}

        TTSReqInternal(const TTSReqInternal &) = delete;
        TTSReqInternal &operator=(const TTSReqInternal &) = delete;

        // since this objetc contains a promise, we have to provide
        // move operaters to copy it into and out of various queues
        TTSReqInternal(TTSReqInternal &&request) : _data(request._data), _result(std::move(request._result)) {}

        TTSReqInternal &operator=(TTSReqInternal &&req)
        {
            _data = req._data;
            _result = std::move(req._result);
            return *this;
        }

        TTSRequest _data;
        std::promise<bool> _result;
    };

    // Internal function to ensure we do not try to use the API when not
    // initialised
    void checkInitState() const;
    bool cacheEnabled() const { return _audio_cache == nullptr ? false : true; }
    bool jingleEnabled() const { return _jingle_mgr == nullptr ? false : true; }

    // Function for the worker thread to run on. This thread brings together
    // all the sub components into a single loop to convert and playback
    // audio speech
    void mainLoop();

    // Thread run state
    std::atomic<bool> _exit_thread;

    // The main worker thread. Takes queued requests from _request_queue and turns
    // them into speech
    std::thread _worker_thread;

    // Queue of requests to be processed as text to speech on the
    // mainloop thread
    DataQueue<TTSReqInternal> _request_queue;

    // this mutext is used to toggle options in the Runner, for
    // example, changing cache locations, jingle locations etc
    mutable std::mutex _access_mutex;

    // TTS functionality, all as pointers since we want to create
    // the various objects at init point and validate them then
    FileTTSCache *_audio_cache;
    PollyTTSDriver *_polly_driver;
    PulseAudioSink *_audio_sink;
    JingleMgr *_jingle_mgr;

    // some general settings and variables
    std::atomic<bool> _init_complete;
    std::atomic<bool> _cancel_current_tts;
    std::atomic<int> _speech_delay;
    std::string _fallback_message;

    // stats and debug
    std::atomic<unsigned long> _cache_hits;
    std::atomic<unsigned long> _polly_requests;
};

} // namespace TTSLibrary

#endif // _TTS_RUNNER_HPP
