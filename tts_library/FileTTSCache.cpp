// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#include "FileTTSCache.hpp"
#include "Log.h"
#include "TTSDataTypes.hpp"
#include "TTSDefines.hpp"

#include <algorithm>
#include <exception>
#include <experimental/filesystem>
#include <fstream>
#include <sstream>

namespace fs = std::experimental::filesystem;
using namespace std;
using namespace LoggingUtils;

namespace TTSLibrary
{
//=============================================================================
//=============================================================================
FileTTSCache::FileTTSCache(const string &cache_location, bool create) : _cache_location(cache_location)
{
    if (_cache_location.empty())
    {
        auto err = CREATE_ERROR("Cannot use a blank string as the cache location");
        LOG(Error) << err << endl;
        throw invalid_argument(err);
    }

    if (_cache_location == ".")
    {
        auto err = CREATE_ERROR("Cannot use the current directory as the cache location");
        LOG(Error) << err << endl;
        throw invalid_argument(err);
    }

    // check we have a viable directory to use as a cache, or create if required
    if (fs::exists(_cache_location))
    {
        if (!fs::is_directory(_cache_location))
        {
            auto err = CREATE_ERROR("Requested cache location: " + _cache_location + " is invalid or not a directory");

            LOG(Error) << err << endl;
            throw invalid_argument(err);
        }
    }
    else if (create)
    {
        if (!fs::create_directory(cache_location))
        {
            auto err = CREATE_ERROR("Unable to create a file cache directory at location: " + _cache_location);
            LOG(Error) << err << endl;
            throw invalid_argument(err);
        }
    }
    else
    {
        auto err = CREATE_ERROR("There is no usable or creatable cache at location: " + _cache_location);
        LOG(Error) << err << endl;
        throw invalid_argument(err);
    }

    LOG(Debug) << "Cache directory set to: " << _cache_location << endl;
}

//=============================================================================
//=============================================================================
bool FileTTSCache::isCached(const string &text_reference, const std::string &voice) const
{
    return fs::exists(createFileName(text_reference, voice));
}

//=============================================================================
//=============================================================================
string FileTTSCache::cache(const string &text_reference, SharedDataBuffer buffer, const std::string &voice)
{
    TRACE_LOGGER;
    auto filename = createFileName(text_reference, voice);

    if (!fs::exists(_cache_location + "/" + voice) && !fs::create_directories(_cache_location + "/" + voice))
    {
        auto err = CREATE_ERROR("Failed to create directories to voice cache for: " + _cache_location + "/" + voice);
        LOG(Error) << err << endl;
        throw logic_error(err);
    }

    // check if the entry already exists, if it does we do not overwrite it
    if (isCached(text_reference, voice))
    {
        auto err = CREATE_ERROR("Text and audio already cached, can not overwrite: " + filename);
        LOG(Error) << err << endl;
        throw logic_error(err);
    }

    // check if the passed buffer has some data in, we require a valid buffer,
    // otherwise zero sized files will be created
    if (buffer == nullptr || buffer->size() == 0)
    {
        auto err = CREATE_ERROR("The audio buffer is empty and cannot be cached: " + filename);
        LOG(Error) << err << endl;
        throw logic_error(err);
    }

    // push the streambuf to file, this should create an audio file on disk,
    // notice we truncate an existing file
    fstream file;
    file.exceptions(ios::failbit);

    try
    {
        // will throw iso_base::failure exception on failure
        file.open(filename, ios::binary | ios::out | ios::trunc);
        file.write((char *)&(*buffer)[0], sizeof(DataByte) * buffer->size());
    }
    catch (const exception &e)
    {
        auto err = CREATE_ERROR("An unknown error occurred when trying to save an audio buffer cache: " + filename +
            ", error: " + e.what());

        LOG(Error) << err << endl;
        file.close();
        throw runtime_error(err);
    }

    file.close();

    LOG(Debug) << "Cached: <" << text_reference << "> to file: " << filename << endl;
    return filename;
}

//=============================================================================
//=============================================================================
SharedDataBuffer FileTTSCache::retrieveCacheEntry(const string &text_reference, const std::string &voice)
{
    TRACE_LOGGER;
    auto filename = createFileName(text_reference, voice);

    // check the cache entry is consistent, if not, throw an error
    if (!fs::exists(filename))
    {
        auto err = CREATE_ERROR("Request for a cached audio that is not in the cache: " + filename);
        LOG(Error) << err << endl;
        throw logic_error(err);
    }

    ifstream file;
    file.exceptions(ios::failbit);
    SharedDataBuffer buffer;

    try
    {
        // will throw iso_base::failure exception on failure
        file.open(filename, ios::in | ios::binary);

        file.seekg(0, ios::end);
        streampos size = file.tellg();
        file.seekg(0, ios::beg);

        // make sure the file has some data in it
        if (size <= 0)
        {
            auto err = CREATE_ERROR("Requested audio cache file is invalid (size is 0). " +
                "Remove the following file from the cache: " + filename);

            LOG(Error) << err << endl;
            throw runtime_error(err);
        }

        // dark arts, use instream iterators to load directory into the const buffer,
        // as its constructed
        buffer = make_shared<const DataBuffer>((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
    }
    catch (const exception &e)
    {
        auto err = CREATE_ERROR(
            "An unknown error occurred when trying to load audio cache: " + filename + ", error: " + e.what());

        LOG(Error) << err << endl;
        file.close();
        throw runtime_error(err);
    }

    file.close();
    LOG(Debug) << "Retrieved sample for: <" << text_reference << "> from file: " << filename << endl;
    return buffer;
}

//=============================================================================
//=============================================================================
void FileTTSCache::removeCacheEntry(const string &text_reference, const std::string &voice)
{
    TRACE_LOGGER;
    auto filename = createFileName(text_reference, voice);

    // attempt to clean up the memory cache and disk, this will hopefully
    // keep it in sync if someone goes deleting files
    if (fs::exists(filename))
        fs::remove(filename);
    else
    {
        auto err = CREATE_ERROR("Cannot remove non-existent file: " + filename);
        LOG(Error) << err << endl;
        throw logic_error(err);
    }
}

//=============================================================================
//=============================================================================
bool FileTTSCache::clearCache()
{
    TRACE_LOGGER;

    // just remove and recreate the cache directory, simple and quick
    fs::remove_all(_cache_location);
    fs::create_directory(_cache_location);
    return true;
}

//=============================================================================
//=============================================================================
int FileTTSCache::cacheSize() const
{
    int entries = std::count_if(fs::recursive_directory_iterator(_cache_location), fs::recursive_directory_iterator(),
        static_cast<bool (*)(const fs::path &)>(fs::is_regular_file));

    return entries;
}

//=============================================================================
//=============================================================================
string FileTTSCache::createFileName(const string &text_reference, const std::string &voice) const
{
    size_t hash_num = hash<string>{}(text_reference);
    stringstream ss;
    ss << _cache_location << "/" << voice << "/" << hash_num;
    return ss.str();
}

} // namespace TTSLibrary
