// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TTS_DEFINES_HPP
#define _TTS_DEFINES_HPP

#include <aws/core/Aws.h>
#include <aws/polly/model/LanguageCode.h>
#include <string>

namespace TTSLibrary
{
// General App Defines
const std::string AppName = "PollyTTSDeviceServer";

// Macro to wrap up some text to build a nice exception string, and allow us to quickly
// trace down errors
#define CREATE_ERROR(str)                                                                                              \
    std::string(std::strrchr("/" __FILE__, '/') + 1) + ":" + string(__func__) + ":" + std::to_string(__LINE__) + " " + \
        str

// AWS Defines/Config
const std::vector<Aws::Polly::Model::LanguageCode> LanguageCodes = {
    Aws::Polly::Model::LanguageCode::en_GB, Aws::Polly::Model::LanguageCode::en_US};

const Aws::Utils::Logging::LogLevel AwsLoggingLevel = Aws::Utils::Logging::LogLevel::Off;

// TTSRunner Tuning
const int MsSpeechDelayDefault = 350;
const int RequestQueuePopTimeout = 100;

} // namespace TTSLibrary

#endif // _TTS_DEFINES_HPP
