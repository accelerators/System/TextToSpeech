// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of TextToSpeech
//
// TextToSpeech is free software: you can redistribute it and/or modify
// it under the terms of the Lesser GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TextToSpeech is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
// GNU General Public License for more details.
//
// You should have received a copy of the Lesser GNU General Public License
// along with TextToSpeech.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _FILE_TTS_CACHE_HPP
#define _FILE_TTS_CACHE_HPP

#include "TTSDataTypes.hpp"
#include <list>
#include <sstream>
#include <string>

namespace TTSLibrary
{
// Simple caching mechanism for the audio speech files generated from AWS.
// We simply store them on disk as raw pcm files, so they can be loaded
// and used at a later point. To work correctly, the cache needs to know
// which voice is in use for each request.
//
// It would be possible to improve this with some simple in memory
// caching of files. This was removed for now, to keep it simple
// and consistent
class FileTTSCache
{
public:
    // Must be valid, unless the option to create it is given
    FileTTSCache(const std::string &cache_location, bool create);
    ~FileTTSCache() {}

    // Check is this text fragment is cached for the given voice
    bool isCached(const std::string &text_reference, const std::string &voice) const;

    // Cache the fragment to disk under the given voices directory
    std::string cache(const std::string &text_reference, SharedDataBuffer buffer, const std::string &voice);

    // Retrieve an audio sample from disk for the given voice
    SharedDataBuffer retrieveCacheEntry(const std::string &text_reference, const std::string &voice);

    // Delete the entry for the given voice
    void removeCacheEntry(const std::string &text_reference, const std::string &voice);

    // Delete all cached files on disk for all voices
    bool clearCache();

    // Utility functions
    int cacheSize() const;
    std::string cacheLocation() const { return _cache_location; }
    void cacheLocation(std::string cache_location);

private:
    // utility functions for the cache
    std::string createFileName(const std::string &text_reference, const std::string &voice) const;

    // stub to the cache location, directories are created below this
    // for different voices
    std::string _cache_location;
};

} // namespace TTSLibrary

#endif // _FILE_TTS_CACHE_HPP
