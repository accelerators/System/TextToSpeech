# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.2] - 2018-06-04

### Fixed

* When no HTTP_PROXY was set in the local environment, the device server would crash on starting.

### Changed

* Moved proxy setting into scripts/setup.sh for convenience.
* Updated README a little to reflect new settings and better explain install

## [1.3.1] - 2018-05-02

### Fixed

* Put get_proxy_settings into a shared general function, and ensured it is not set to null when getenv does not detect the HTTP_PROXY environment setting. This fixes exceptions in the unit tests when incorrectly configured.

## [1.3.0] - 2018-04-30

### Added

* Proxy is now read from the HTTP_PROXY environment variable, rather than a hard coded value.

### Fixed

* Unit test build debug/trace flags now work correctly.

## [1.2.0] - 2018-03-28

### Added

* Added a fallback message to be played when the server can not contact Amazon Web Services. 
  * Added auto generation of the fallback speech in TTSRunner cache initalisation. Not available without the cache.
  * Added property to TextToSpeech device server to configure the actual message.
* Added various stats counters for the expert view
  * Added a debug_watch_thread thread to the TextToSpeech device server to wait on the new results futures. This adjusts the stats as it reads results.
  * Saved stats between runs and push events for archiving in code.
* The TTSRunner now returns a future to communicate the result of the speech request back to the TextToSpeech device server.

### Changed

* Some improvements to the build system.
  * Enabled debug in unit tests. 
  * Custom catch main to switch on debug at WARNING for when enabled.
  * Improved handling of the pkgconfig paths
* Merged some logging tools and cleaned them up.
* Improved tango class errors and exceptions.
* Clang-format pass.

## [1.1.0] - 2017-12-13

### Added

* Ability to configure millisecond delay between jingle end and speech start.
* Pushed text_to_talk attribute event on every write (even when they are the same)
* Some additional error checking in the device server.

### Changed

* Made text_to_talk READ/WRITE (was READ only).

### Removed

## [1.0.0] - 2017-12-12

### Added

* Initial import of project
* Added tts_library, device server, unit tests, scripts, jingles
